import os
import json
import logging
import sentry_sdk

host = os.environ.get("HOST") or os.environ.get("AGENT_HOST") or "0.0.0.0"
port = os.environ.get("PORT") or os.environ.get("AGENT_PORT") or "8000"
telegram_token = os.environ.get("AGENT_TELEGRAM_TOKEN")
storage = os.environ.get("AGENT_STORAGE", "mongodb://localhost/service")
logging_level = os.environ.get("AGENT_LOGGING_LEVEL") or "DEBUG"
api_secret = os.environ.get("AGENT_API_SECRET", "")
sentry_url = os.environ.get("SENTRY_URL", None)

check_black_list_timeout = 1 * 60
pull_interval = 2 * 60
apartment_ttl = 7 * 24 * 60 * 60 # seconds

def setup():
    # disable requests logging
    logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging_level)
    logging.basicConfig(format="%(asctime)s %(name)s %(message)s", level=logging_level)

    if sentry_url:
        logging.debug("intialized Sentry")
        sentry_sdk.init(sentry_url)