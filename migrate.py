from lib import storage
import config
import logging
import sys
import os
import uuid

def migrate(collection="subscriptions"):
    src = storage.Storage(collection, os.environ.get("SRC_URI"))
    dst = storage.Storage(collection, os.environ.get("DST_URI"))

    for k,v in src.items():
        if type(v) is list:
            for i in v:
                dst.set(uuid.uuid4(), i)
        else:
            dst.set(uuid.uuid4(), v)
    print(dst)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    migrate()