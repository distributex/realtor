#!/usr/bin/env python3

import argparse
import json
import logging, logging.handlers
import time
import os
import sys
import datetime
from os import path

import config
from lib import feeds, subway
from lib.storage import Storage
from lib.subscription import SubscriptionService
from lib.objects import Apartment, Type, Subscription
from lib.apartments import Apartments

PHONE_RECROD_TIME_TO_LIVE=24*60*60
PHONE_RECORDS_ALLOWED=2

class Crawler(object):
    def __init__(self, config):
        self.phones_usage_storage = Storage('ignored_phones')
        self.phones_usage_storage.set_ttl("created_at", PHONE_RECROD_TIME_TO_LIVE)
        self.crawler_pool = Storage('crawler_pool')
        self.subscription_service = SubscriptionService(config)
        self.config = config
        self.apartments = Apartments(config)

    def on_new_apartment(self, apartment):
        for phone in apartment.phones:
            self.phones_usage_storage.set(None, {"phone": phone, "created_at": datetime.datetime.utcnow()})

        apartment.subways = subway.get_nearest(apartment.location.latitude, apartment.location.longitude)
        self.crawler_pool.set(apartment.id, apartment)
        return True

    def notify_subscribers(self):
        now = int(time.time())
        for apartment in self.crawler_pool.values():
            logging.debug('pulling out %s', apartment.id)
            ts = apartment.ts

            if now - ts > self.config.check_black_list_timeout:
                # ignore agents only for the rented property
                if apartment.type in (Type.RENT_FLAT, Type.RENT_ROOM):
                    for phone in apartment.phones:
                        count = self.phones_usage_storage.count({"phone": phone})
                        if count > PHONE_RECORDS_ALLOWED:
                            logging.warning("%s looks like agent phone" % phone)
                            break
                    else:
                        continue

                logging.info('sending announce %s', apartment.id)
                self.subscription_service.announce_all(apartment)
                self.apartments.append(apartment)
                self.crawler_pool.delete(apartment.id)

def main(options):
    config.setup()
    crawler = Crawler(config)

    pulllers_config = json.load(open('pullers.json', 'r'))
    pullers = [feeds.create(c, Storage('pullers')) for c in pulllers_config['pullers']]
    logging.info('initialized %d feeds', len(pullers))

    if not options.disable_pulling:
        for p in pullers:
            try:
                p.pull(crawler.on_new_apartment)
            except (ConnectionError) as e:
                logging.error('failed to pull feed %r' % e)

    crawler.notify_subscribers()
    crawler.apartments.gc()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--disable-pulling', action='store_true')
    options = parser.parse_args()
    main(options)
