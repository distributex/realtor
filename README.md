# Description
A [Telegram-bot Realtor](https://t.me/estate_agent_bot) that pulls [rent.onliner.by](https://rent.onliner.by) and notify you about new ready-for-sale/ready-for-lease apartments.

# Contribute
Feel free to contribute to this project. Any help is appreciated. Just create a pull-request or email us. You can get an email from the commit history ;)

It is good to extend scrappers for [realt.by](https://realt.by) and [neagent.by](https://neagent.by).

# Development
## Dependencies
```
apt-get install -y -qq python3 python3-pip
pip3 install -r requirements.txt

```
## Testing
```sh
pytest
```

## Deploying to heroku
```sh
heroku login
heroku git::remote --app realtor
git push heroku master
```

## Microservices architecture
### api.py
Expose REST API and Web Hook

### crawler.py
Runs by scheduler, pull data from source sites and notify subscribers about the new options