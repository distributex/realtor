from lib import storage
from lib.subscription import SubscriptionService
import config
import logging

def migrate(config):
    service = SubscriptionService(config)
    storage = service.subscriptions
    for sid, subscriptions in storage.items():
        s = subscriptions[0]
        service = SubscriptionService(config)
        service.send_message(s, 'Я научился подыскивать квартиры для покупки!')

if __name__ == "__main__":
    migrate(config)