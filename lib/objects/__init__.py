import json
import os
import hashlib
import inspect
from enum import Enum
import sys

class Serializable(object):
    def __repr__(self):
        return repr(self.__dict__)
    def migrate(self):
        pass

class Location(Serializable):
    __type__ = 'location'

    def __init__(self, data=None, address=None, latitude=None, longitude=None):
        self.address = address
        self.latitude = latitude
        self.longitude = longitude


class Type(object):
    RENT_FLAT = "rent_flat"
    RENT_ROOM = "rent_room"
    SELL = "sell"


class Area(Serializable):
    __type__ = 'area'

    def __init__(self):
        self.total = None
        self.living = None
        self.kitchen = None

class Apartment(Serializable):
    __type__ = 'apartment'

    def __init__(self, data=None):
        self.location = Location()
        self.floor = None
        self.rooms = None
        self.price = None
        self.currency = None
        self.link = None
        self.phones = []
        self.photos = []
        self.type = None
        self.ts = None
        self.id = None
        self.area = Area()
        self.subways = []


class Subscription(Serializable):
    __type__ = 'subscriber'

    def __init__(self,
                 data=None,
                 city=None,
                 address=None,
                 distance=None,
                 budget=None,
                 rooms=None,
                 currency='USD',
                 location=None,
                 subscription_type=Type.RENT_FLAT):
        self.city = city
        self.address = address
        self.distance = distance
        self.budget = budget
        self.currency = currency
        self.rooms = rooms
        self.location = location
        self.subscription_type = subscription_type
        self.source = None
        self.chat_id = None
        self.username = None

    def migrate(self):
        if hasattr(self, "subscription_types"):
            self.subscription_type = getattr(self, "subscription_types")[0]
            del self.__dict__["subscription_types"]

class User(Serializable):
    __type__ = "user"
    def __init__(self, name, source, chat_id, first_name=None, last_name=None):
        self.name = name
        self.source = source
        self.chat_id = chat_id
        self.first_name = first_name
        self.last_name = last_name

types = {m[1].__type__: m[1] for m in inspect.getmembers(sys.modules[__name__], inspect.isclass) if hasattr(m[1], '__type__')}