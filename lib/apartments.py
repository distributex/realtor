
from .storage import Storage
from .objects import Apartment
import time
import logging

class Apartments(object):
    def __init__(self, config):
        self.apartment_ttl = config.apartment_ttl
        self.storage = Storage('apartments')

    def append(self, apartment):
        self.storage.set(apartment.id, apartment)
    
    def gc(self):
        now = int(time.time())

        for apartment_id, apartment in self.storage.items():
            if now - apartment.ts > self.apartment_ttl:
                logging.info('deleting apartment %s due to ttl expiration: now=%d, apartment.ts=%d, ttl=%d', apartment_id, now, apartment.ts, self.apartment_ttl)
                self.storage.delete(apartment_id)


    def forEach(self, predicate):
        now = int(time.time())

        for apartment_id, apartment in self.storage.items():
            if now - apartment.ts > self.apartment_ttl:
                logging.info('deleting apartment %s due to ttl expiration: now=%d, apartment.ts=%d, ttl=%d', apartment_id, now, apartment.ts, self.apartment_ttl)
                self.storage.delete(apartment_id)
                continue

            predicate(apartment)