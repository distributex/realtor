import json
import os
import logging
import copy
from geopy import distance as GeoDistance
from geopy.geocoders import Nominatim as Geolocator

with open(os.path.join(os.path.dirname(__file__), "data", "subways.json"), "r") as f:
    _subways = json.load(f)

def get_nearest(lat, lon, distance_m = 1500):
    subways = []
    # TODO: Nominatim API is blocking requests
    # geolocator = Geolocator(country_bias="US")
    # r = geolocator.reverse((lat, lon))
    # city, country = r.raw["address"]["city"], r.raw["address"]["country"]
    city, country = "Минск", "Беларусь"
    
    if country in _subways and city in _subways[country]:
        for subway in _subways[country][city]:
            subway_distance = int(GeoDistance.distance((subway["lat"], subway["lon"]), (lat, lon)).meters)
            if subway_distance <= distance_m:
                s = copy.copy(subway)
                s["distance"] = subway_distance
                subways.append(s)

    return subways