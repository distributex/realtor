import logging
import uuid
from pymongo import MongoClient
from os import path
from . import objects

def marshal(obj):
    if hasattr(obj, '__type__'):
        o = {'__type__': obj.__type__}
        for key, value in obj.__dict__.items():
            o[key] = marshal(value)
        return o
    else:
        return obj

def unmarshal(obj):
    if type(obj) is dict and '__type__' in obj:
        o = objects.types[obj['__type__']]()
        for k,v in obj.items():
            o.__dict__[k] = unmarshal(v)
        o.migrate()
        return o

    return obj

class Storage(dict):
    def __init__(self, uri, name):
        logging.info(f"using storage \"{name}\" in \"{uri}\"")
        client = MongoClient(uri)
        self.name = name
        self.db = client.get_default_database()
        self.collection = self.db[self.name]

    def set(self, key, value):
        return self.collection.update_one(
            {"_id": key if key is not None else uuid.uuid4()},
            {"$set": marshal(value)},
            upsert=True
        )

    def get(self, key):
        return next(self.collection.find({"_id": key}), None)

    def delete(self, key):
        self.collection.delete_one({'_id': key})

    def delete_many(self, f={}):
        return self.collection.delete_many(f)

    def values(self, f={}):
        for item in self.collection.find(f):
            yield unmarshal(item)

    def keys(self):
        for item in self.__iter__():
            yield item._id

    def items(self):
        for item in self.__iter__():
            yield (item._id, item)

    def __setitem__(self, key, value):
        return self.set(key, value)

    def __getitem__(self, key):
        return self.get(key)

    def __iter__(self):
        for item in self.collection.find({}):
            yield unmarshal(item)

    def __delitem__(self, key):
        self.delete(key)

    def drop(self):
        return self.collection.drop()

    def count(self, query):
        return self.collection.count_documents(query)
    
    def set_ttl(self, query: str, ttl: int):
        return self.collection.create_index(query, expireAfterSeconds=ttl)