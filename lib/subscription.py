import logging
import json
import requests
from geopy.geocoders import Nominatim as Geolocator
from geopy import distance as GeoDistance
from lib.objects import Subscription, Type, Location, User
from .storage import Storage

def apartment_offer_type_to_string(t):
    if t == Type.RENT_FLAT:
        return 'аренда квартиры'
    elif t == Type.RENT_ROOM:
        return 'аренда комнаты'
    elif t == Type.SELL:
        return 'продажа квартиры'
    else:
        return ''

def get_subscription_action(s):
    if Type.RENT_FLAT == s.subscription_type or Type.RENT_ROOM == s.subscription_type:
        return 'aрендовать'
    elif Type.SELL == s.subscription_type:
        return 'купить'

class SubscriptionService(object):
    def __init__(self, config, send_message_cb=None):
        self.send_message_cb = send_message_cb or self.send_message
        self.config = config
        self.subscriptions = Storage("subscriptions")
        self.users = Storage("clients")

    def subscribe(self, city, address, distance, budget, currency, rooms, source, chat_id, username, subscription_type):
        s = Subscription(city=city, address=address, distance=distance, budget=budget, rooms=rooms, currency=currency)
        s.source = source
        s.chat_id = chat_id
        s.username = username
        s.subscription_type = subscription_type

        location = Geolocator(user_agent='fedor').geocode(city + ' ' + (address if address else ' '))

        if location is not None:
            s.location = Location(address=location.address, latitude=location.latitude, longitude=location.longitude)
            self.subscriptions.set(None, s)
            self.users.set(f"{source}-{chat_id}", User(username, source, chat_id))
            self.send_message_cb(s, 'Отлично! Я буду пытаться подыскать что-то подходящее (квартира на расстоянии {distance} км от \"{location.address}\" с бюджетом {budget} {currency}) и сообщать тебе.'.format(**s.__dict__))
            return s
        else:
            self.send_message_cb(s, 'Извините, не могу найти \"%s %s\"' % (city, address))
            return None

    def announce_subscriptions(self, source, chat_id, username):
        has_subscritions = False
        for subscription in self.subscriptions.values({"chat_id": chat_id, "source": "telegram"}):
            text = 'Вы хотите {action} квартиру на расстоянии {distance} км от \"{location.address}\" с бюджетом {budget} {currency}.'.format(action=get_subscription_action(subscription), **subscription.__dict__)
            self.send_message_cb(subscription, text)
            has_subscritions = True
        # if not has_subscritions:
        #     self.send_message_cb(subscription, f"У вас нету подписок!")

    def unsubscribe(self, source, chat_id):
        logging.info("Unsubscribing %s %d", source, chat_id)
        self.subscriptions.delete_many({"chat_id": chat_id, "source": source})

    def send_message(self, subscription, text):
        if subscription.source == "telegram":
            p = {"text": text, "chat_id": subscription.chat_id}
            r = requests.post("https://api.telegram.org/bot{token}/sendMessage?parse_mode=Markdown".format(token=self.config.telegram_token), json=p)
            logging.debug("send_message(%s): telegram api response %r", p, r.json())
        else:
            logging.error('no supported messanger %s', subscription.source)

    @staticmethod
    def match(apartment, subscriber):
        if apartment.type != subscriber.subscription_type and not (apartment.type == Type.RENT_ROOM and subscriber.subscription_type == Type.RENT_FLAT and subscriber.rooms == 1):
            return False, None

        distance_km = GeoDistance.distance((subscriber.location.latitude, subscriber.location.longitude),
            (apartment.location.latitude, apartment.location.longitude)).kilometers

        if distance_km > subscriber.distance:
            return False, None

        if float(apartment.price) > float(subscriber.budget):
            return False, None

        if int(apartment.rooms) < int(subscriber.rooms):
            return False, None

        return True, distance_km

    @staticmethod
    def format_announce(apartment, subscription):
        distance_km = GeoDistance.distance((subscription.location.latitude, subscription.location.longitude),
            (apartment.location.latitude, apartment.location.longitude)).kilometers

        message = (
                    "🏠 {t}, {rooms} комната(ы)\n"
                    "💲 {price} {currency}\n"
                    "🚶‍♂️ {distance:.2f} км,\n"
                    "🗺 {location.address}\n"
                    "📷 [Фото]({photo})\n"
                    "🌍 [Подробнее]({link})\n"
                ).format(
                    distance=distance_km,
                    t=apartment_offer_type_to_string(apartment.type),
                    photo=apartment.photos[0] if len(apartment.photos) > 0 else 'Нет фото',
                    **apartment.__dict__
                )

        if len(apartment.phones) > 0:
            message += "📱 %s\n" % (', '.join(apartment.phones))

        if apartment.area.total is not None:
            message += f"🧱 {apartment.area.total} м2\n"
            message += f"💲 {int(apartment.price/apartment.area.total)} $/м2\n"
        if len(apartment.subways) > 0:
            message += "🚇 " + ', '.join([f'{subway["name"]} {subway["distance"]} м' for subway in apartment.subways])

        return message

    def announce(self, subscription, apartment):
        match, distance_km = self.match(apartment, subscription)
        if match:
            message = self.format_announce(apartment, subscription)
            self.send_message_cb(subscription, message)

    def announce_all(self, apartment):
        for subscription in self.subscriptions.values():
            self.announce(subscription, apartment)