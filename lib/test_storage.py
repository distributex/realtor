import pytest
import uuid
from .storage import Storage
import logging
import os

objects_count = 0

@pytest.yield_fixture(scope='module')
def storage():
    storage = Storage("test")
    yield storage
    storage.drop()

def test_set(storage):
    global objects_count
    object_key = uuid.uuid4()
    logging.debug(f'key is {object_key}')
    storage.set(object_key, {'foo': 'bar'})
    o = storage.get(object_key)
    assert o['foo'] == 'bar'
    objects_count += 1

def test_delete(storage):
    key = uuid.uuid4()
    storage.set(key, {'foo': 'bar'})
    storage.delete(key)
    assert storage.get(key) is None

def test_iteration(storage):
    i = 0
    for item in storage:
        logging.info("%s", item)
        i += 1
    assert i == objects_count

def test_values(storage):
    i = 0
    for item in storage.values():
        logging.info("%s", item)
        i += 1
    assert i == objects_count