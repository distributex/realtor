from . import onliner

api_response = """
{
    "apartments": [
        {
            "id": 320112,
            "author_id": 78730,
            "location": {
                "address": "Минск, улица Скрипникова, 72",
                "user_address": "Минск, улица Скрипникова, 72",
                "latitude": 53.905602,
                "longitude": 27.425726
            },
            "price": {
                "amount": "98000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "219480.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "98000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 3,
            "floor": 4,
            "number_of_floors": 19,
            "area": {
                "total": 78.1,
                "living": 45.2,
                "kitchen": 9.5
            },
            "photo": "https://content.onliner.by/apartment_for_sale/78730/600x400/b0159e4cff528403ca78da85eeaff64b.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-01-12T15:57:59+0300",
            "last_time_up": "2020-03-06T18:06:32+0300",
            "up_available_in": 45350,
            "url": "https://r.onliner.by/pk/apartments/320112",
            "auction_bid": {
                "amount": "8.00",
                "currency": "BYN"
            }
        },
        {
            "id": 334788,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Плеханова ул. 65",
                "user_address": "Минск, Плеханова ул. 65",
                "latitude": 53.862801,
                "longitude": 27.612801
            },
            "price": {
                "amount": "54000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "120938.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "54000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 6,
            "number_of_floors": 9,
            "area": {
                "total": 33,
                "living": 17.1,
                "kitchen": 7.2
            },
            "photo": "https://static.realt.by/user/i4/g/site16fbhgi4/14dedba7d1d2a48a.jpg?1583317290",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-01T09:58:22+0300",
            "last_time_up": "2020-03-04T13:21:04+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/334788",
            "auction_bid": {
                "amount": "5.00",
                "currency": "BYN"
            }
        },
        {
            "id": 329276,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Лобанка ул. 22",
                "user_address": "Минск, Лобанка ул. 22",
                "latitude": 53.901402,
                "longitude": 27.434
            },
            "price": {
                "amount": "97500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "218361.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "97500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 8,
            "number_of_floors": 9,
            "area": {
                "total": 58.3,
                "living": 31.1,
                "kitchen": 12.7
            },
            "photo": "https://static.realt.by/user/hq/9/site15l7c9hq/87db48c13d2a77df.jpg?1581509654",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-12T14:50:04+0300",
            "last_time_up": "2020-03-05T11:22:08+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/329276",
            "auction_bid": {
                "amount": "5.00",
                "currency": "BYN"
            }
        },
        {
            "id": 326288,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Независимости просп. 168к1",
                "user_address": "Минск, Независимости просп. 168к1",
                "latitude": 53.9445,
                "longitude": 27.689199
            },
            "price": {
                "amount": "209000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "468076.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "209000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 11,
            "number_of_floors": 12,
            "area": {
                "total": 148.2,
                "living": 85.7,
                "kitchen": 19.2
            },
            "photo": "https://static.realt.by/user/1m/4/site1547g41m/c2b200b67a3694f9.jpg?1580715138",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-03T11:44:18+0300",
            "last_time_up": "2020-03-05T13:13:50+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/326288",
            "auction_bid": {
                "amount": "4.50",
                "currency": "BYN"
            }
        },
        {
            "id": 331483,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Звязда газеты просп. 22к1",
                "user_address": "Минск, Звязда газеты просп. 22к1",
                "latitude": 53.864517,
                "longitude": 27.46583
            },
            "price": {
                "amount": "80500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "180287.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "80500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 9,
            "number_of_floors": 9,
            "area": {
                "total": 63.5,
                "living": 43.8,
                "kitchen": 7
            },
            "photo": "https://static.realt.by/user/q7/h/site15xwvhq7/b59b27d6603b7208.jpg?1582101150",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-19T11:34:02+0300",
            "last_time_up": "2020-03-05T11:22:03+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/331483",
            "auction_bid": {
                "amount": "4.50",
                "currency": "BYN"
            }
        },
        {
            "id": 331256,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Бачило ул. 30",
                "user_address": "Минск, Бачило ул. 30",
                "latitude": 53.843102,
                "longitude": 27.6973
            },
            "price": {
                "amount": "55000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "123178.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "55000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 1,
            "number_of_floors": 9,
            "area": {
                "total": 52,
                "living": 30.1,
                "kitchen": 9.3
            },
            "photo": "https://static.realt.by/user/qo/c/site15wabcqo/efd5ff05f8b9768e.jpg?1583253729",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-18T14:28:03+0300",
            "last_time_up": "2020-03-05T11:22:04+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/331256",
            "auction_bid": {
                "amount": "4.00",
                "currency": "BYN"
            }
        },
        {
            "id": 301524,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Никитина ул. 21",
                "user_address": "Минск, Никитина ул. 21",
                "latitude": 53.951,
                "longitude": 27.5933
            },
            "price": {
                "amount": "199900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "447696.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "199900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 4,
            "floor": 3,
            "number_of_floors": 3,
            "area": {
                "total": 146.6,
                "living": 100.6,
                "kitchen": 10.8
            },
            "photo": "https://static.realt.by/user/91/n/site1zrskn91/d21ead98c46d0277.jpg?1571737907",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-10-22T12:53:03+0300",
            "last_time_up": "2020-03-05T11:24:41+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/301524",
            "auction_bid": {
                "amount": "4.00",
                "currency": "BYN"
            }
        },
        {
            "id": 327520,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Сухаревская ул. 21к2",
                "user_address": "Минск, Сухаревская ул. 21к2",
                "latitude": 53.886299,
                "longitude": 27.4338
            },
            "price": {
                "amount": "85000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "190366.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "85000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 1,
            "number_of_floors": 10,
            "area": {
                "total": 64.2,
                "living": 37.9,
                "kitchen": 8.9
            },
            "photo": "https://static.realt.by/user/bl/2/site15a2u2bl/886cc0b116351180.jpg?1581491469",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-06T14:41:03+0300",
            "last_time_up": "2020-03-05T11:22:17+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/327520",
            "auction_bid": {
                "amount": "4.00",
                "currency": "BYN"
            }
        },
        {
            "id": 332423,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Лучины ул. 46",
                "user_address": "Минск, Лучины ул. 46",
                "latitude": 53.839199,
                "longitude": 27.5842
            },
            "price": {
                "amount": "55000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "123178.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "55000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 4,
            "number_of_floors": 9,
            "area": {
                "total": 37.2,
                "living": 17.1,
                "kitchen": 8.9
            },
            "photo": "https://static.realt.by/user/19/8/site161y8819/cd6bcc179a6071db.jpg?1583411861",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-21T15:51:10+0300",
            "last_time_up": "2020-03-05T15:38:02+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/332423",
            "auction_bid": {
                "amount": "4.00",
                "currency": "BYN"
            }
        },
        {
            "id": 335013,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Ангарская ул. 18к1",
                "user_address": "Минск, Ангарская ул. 18к1",
                "latitude": 53.866199,
                "longitude": 27.681801
            },
            "price": {
                "amount": "66500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "148933.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "66500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 2,
            "number_of_floors": 5,
            "area": {
                "total": 49.1,
                "living": 33.9,
                "kitchen": 5.6
            },
            "photo": "https://static.realt.by/user/o5/k/site16k2zko5/5e1f25bf1bd10208.jpeg?1583399065",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-02T13:45:25+0300",
            "last_time_up": "2020-03-06T08:53:22+0300",
            "up_available_in": 12160,
            "url": "https://r.onliner.by/pk/apartments/335013",
            "auction_bid": {
                "amount": "4.00",
                "currency": "BYN"
            }
        },
        {
            "id": 335972,
            "author_id": 1946356,
            "location": {
                "address": "Минск, Литературная улица, 22",
                "user_address": "Минск, Литературная улица, 22",
                "latitude": 53.940781,
                "longitude": 27.595871
            },
            "price": {
                "amount": "77900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "174464.84",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "77900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 8,
            "number_of_floors": 25,
            "area": {
                "total": 47.7,
                "living": 17.4,
                "kitchen": 12
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1946356/600x400/3ac39001ef0be0c97271544a35917a68.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-03T16:00:32+0300",
            "last_time_up": "2020-03-06T17:34:45+0300",
            "up_available_in": 43443,
            "url": "https://r.onliner.by/pk/apartments/335972",
            "auction_bid": {
                "amount": "3.50",
                "currency": "BYN"
            }
        },
        {
            "id": 324952,
            "author_id": 2837884,
            "location": {
                "address": "Минск, Нововиленская улица, 24",
                "user_address": "Минск, Нововиленская улица, 24",
                "latitude": 53.932915,
                "longitude": 27.538326
            },
            "price": {
                "amount": "39900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "89360.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "39900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 3,
            "number_of_floors": 3,
            "area": {
                "total": 25.3,
                "living": 17.7,
                "kitchen": 4.2
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2837884/600x400/d86bb5b7b66e57e1ef5732d7f35a27fe.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-29T10:54:45+0300",
            "last_time_up": "2020-03-05T17:56:55+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/324952",
            "auction_bid": {
                "amount": "3.50",
                "currency": "BYN"
            }
        },
        {
            "id": 336921,
            "author_id": 2975005,
            "location": {
                "address": "Минск, Братская улица, 12",
                "user_address": "Минск, Братская улица, 12",
                "latitude": 53.868645,
                "longitude": 27.551754
            },
            "price": {
                "amount": "77000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "172449.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "77000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 15,
            "number_of_floors": 21,
            "area": {
                "total": 49.1,
                "living": 38.1,
                "kitchen": 11
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2975005/600x400/98de89f31523752547493faa191643e0.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-06T16:41:09+0300",
            "last_time_up": "2020-03-06T16:41:09+0300",
            "up_available_in": 40227,
            "url": "https://r.onliner.by/pk/apartments/336921",
            "auction_bid": {
                "amount": "3.50",
                "currency": "BYN"
            }
        },
        {
            "id": 322385,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Купалы ул. 17",
                "user_address": "Минск, Купалы ул. 17",
                "latitude": 53.9035,
                "longitude": 27.5669
            },
            "price": {
                "amount": "139900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "313320.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "139900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 2,
            "number_of_floors": 8,
            "area": {
                "total": 76.6,
                "living": 54.6,
                "kitchen": 9.3
            },
            "photo": "https://static.realt.by/user/54/m/site14el0m54/ff59fee259ad172a.jpg?1583226965",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-20T14:30:04+0300",
            "last_time_up": "2020-03-05T11:22:21+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/322385",
            "auction_bid": {
                "amount": "3.00",
                "currency": "BYN"
            }
        },
        {
            "id": 306328,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Макаенка ул. 12Б",
                "user_address": "Минск, Макаенка ул. 12Б",
                "latitude": 53.922001,
                "longitude": 27.6264
            },
            "price": {
                "amount": "161900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "362591.24",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "161900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 10,
            "number_of_floors": 16,
            "area": {
                "total": 90.7,
                "living": 70,
                "kitchen": 20
            },
            "photo": "https://static.realt.by/user/o9/c/site10uxgco9/a00b86f7175b7fd3.jpg?1573563951",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-11-12T16:07:03+0300",
            "last_time_up": "2020-03-05T11:24:36+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/306328",
            "auction_bid": {
                "amount": "3.00",
                "currency": "BYN"
            }
        },
        {
            "id": 324475,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Козлова пер. 20",
                "user_address": "Минск, Козлова пер. 20",
                "latitude": 53.899399,
                "longitude": 27.598301
            },
            "price": {
                "amount": "66500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "148933.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "66500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 2,
            "number_of_floors": 5,
            "area": {
                "total": 42.3,
                "living": 26.3,
                "kitchen": 6.2
            },
            "photo": "https://static.realt.by/user/3n/m/site14rqnm3n/9f9296112b620865.jpg?1583236850",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-27T16:58:03+0300",
            "last_time_up": "2020-03-05T11:22:26+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/324475",
            "auction_bid": {
                "amount": "3.00",
                "currency": "BYN"
            }
        },
        {
            "id": 326945,
            "author_id": 2837884,
            "location": {
                "address": "Минск, Калининградский переулок, 21",
                "user_address": "Минск, Калининградский переулок, 21",
                "latitude": 53.929108,
                "longitude": 27.600595
            },
            "price": {
                "amount": "93900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "210298.44",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "93900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 2,
            "number_of_floors": 12,
            "area": {
                "total": 58.9,
                "living": 29.8,
                "kitchen": 9.6
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2837884/600x400/e6bc5256e0ce7bb5669a2f86bc6544e4.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-05T09:21:24+0300",
            "last_time_up": "2020-03-03T08:23:35+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/326945",
            "auction_bid": {
                "amount": "3.00",
                "currency": "BYN"
            }
        },
        {
            "id": 336553,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Ангарская ул. 15к1",
                "user_address": "Минск, Ангарская ул. 15к1",
                "latitude": 53.865036,
                "longitude": 27.678673
            },
            "price": {
                "amount": "66000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "147813.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "66000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 5,
            "number_of_floors": 5,
            "area": {
                "total": 64.5,
                "living": 44.5,
                "kitchen": 6.6
            },
            "photo": "https://static.realt.by/user/ca/3/site16pvt3ca/a019aa28b4b498e7.jpg?1583406136",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-05T14:03:02+0300",
            "last_time_up": "2020-03-05T14:03:02+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/336553",
            "auction_bid": {
                "amount": "3.00",
                "currency": "BYN"
            }
        },
        {
            "id": 328418,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Асаналиева ул. 5",
                "user_address": "Минск, Асаналиева ул. 5",
                "latitude": 53.851101,
                "longitude": 27.551001
            },
            "price": {
                "amount": "85000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "190366.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "85000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 9,
            "number_of_floors": 9,
            "area": {
                "total": 63.3,
                "living": 43.4,
                "kitchen": 7.1
            },
            "photo": "https://static.realt.by/user/mx/z/site15h90zmx/3eb1405d368730b8.jpg?1581323652",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-10T11:36:02+0300",
            "last_time_up": "2020-03-05T11:22:16+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/328418",
            "auction_bid": {
                "amount": "3.00",
                "currency": "BYN"
            }
        },
        {
            "id": 337027,
            "author_id": 801828,
            "location": {
                "address": "Боровлянский сельский Совет, Копище, улица Николая Михайлашева, 1",
                "user_address": "Боровлянский сельский Совет, Копище, улица Николая Михайлашева, 1",
                "latitude": 53.961746,
                "longitude": 27.672701
            },
            "price": {
                "amount": "60000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "134376.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "60000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 7,
            "number_of_floors": 10,
            "area": {
                "total": 37.1,
                "living": 25.5,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/801828/600x400/bb495e5ac232d7444c1dd09a89c7a14b.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-07T13:29:12+0300",
            "last_time_up": "2020-03-07T13:29:12+0300",
            "up_available_in": 115110,
            "url": "https://r.onliner.by/pk/apartments/337027",
            "auction_bid": {
                "amount": "3.00",
                "currency": "BYN"
            }
        },
        {
            "id": 288156,
            "author_id": 769039,
            "location": {
                "address": "Минск, проспект Дзержинского",
                "user_address": "Минск, проспект Дзержинского, 26",
                "latitude": 53.887665,
                "longitude": 27.515499
            },
            "price": {
                "amount": "99000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "221720.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "99000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 3,
            "floor": 20,
            "number_of_floors": 23,
            "area": {
                "total": 86,
                "living": 46,
                "kitchen": 13.5
            },
            "photo": "https://content.onliner.by/apartment_for_sale/769039/600x400/178ff7fb8a015a943b7e9199bd5ab21f.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-08-24T14:37:58+0300",
            "last_time_up": "2020-03-05T01:38:10+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/288156",
            "auction_bid": {
                "amount": "2.50",
                "currency": "BYN"
            }
        },
        {
            "id": 334022,
            "author_id": 415383,
            "location": {
                "address": "Минск, улица Якуба Коласа, 30",
                "user_address": "Минск, улица Якуба Коласа, 30",
                "latitude": 53.926315,
                "longitude": 27.594793
            },
            "price": {
                "amount": "80500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "180287.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "80500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 6,
            "number_of_floors": 7,
            "area": {
                "total": 48.4,
                "living": 34.1,
                "kitchen": 7.1
            },
            "photo": "https://content.onliner.by/apartment_for_sale/415383/600x400/97c288986d819daa287ed756c3b4b817.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-27T00:13:19+0300",
            "last_time_up": "2020-03-03T14:08:19+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/334022",
            "auction_bid": {
                "amount": "2.50",
                "currency": "BYN"
            }
        },
        {
            "id": 334299,
            "author_id": 1204134,
            "location": {
                "address": "Минск, улица Владислава Голубка, 10",
                "user_address": "Минск, улица Голубка, 10",
                "latitude": 53.9235,
                "longitude": 27.451681
            },
            "price": {
                "amount": "95000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "212762.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "95000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 3,
            "number_of_floors": 9,
            "area": {
                "total": 72.3,
                "living": 44.8,
                "kitchen": 11.9
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1204134/600x400/266ee7f417803b99b33f5e476a420814.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-27T18:55:26+0300",
            "last_time_up": "2020-03-06T10:56:31+0300",
            "up_available_in": 19549,
            "url": "https://r.onliner.by/pk/apartments/334299",
            "auction_bid": {
                "amount": "2.00",
                "currency": "BYN"
            }
        },
        {
            "id": 293243,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Корженевского ул. 33к2",
                "user_address": "Минск, Корженевского ул. 33к2",
                "latitude": 53.845501,
                "longitude": 27.503099
            },
            "price": {
                "amount": "92000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "206043.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "92000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 8,
            "number_of_floors": 9,
            "area": {
                "total": 80.5,
                "living": 45,
                "kitchen": 12.5
            },
            "photo": "https://static.realt.by/user/re/1/site1xrwj1re/c4769d339d90d142.jpg?1579698777",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-09-13T17:12:03+0300",
            "last_time_up": "2020-03-05T11:24:59+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/293243",
            "auction_bid": {
                "amount": "2.00",
                "currency": "BYN"
            }
        },
        {
            "id": 322799,
            "author_id": 2837884,
            "location": {
                "address": "Минск, улица Притыцкого, 91",
                "user_address": "Минск, улица Притыцкого, 91",
                "latitude": 53.905628,
                "longitude": 27.441881
            },
            "price": {
                "amount": "156500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "350497.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "156500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 5,
            "number_of_floors": 10,
            "area": {
                "total": 91.1,
                "living": 60,
                "kitchen": 13.4
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2837884/600x400/a9f40c0bcd816eaa2f0424ca06d75b1a.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-21T17:22:58+0300",
            "last_time_up": "2020-03-05T17:56:56+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/322799",
            "auction_bid": {
                "amount": "2.00",
                "currency": "BYN"
            }
        },
        {
            "id": 322815,
            "author_id": 2460063,
            "location": {
                "address": "Минск, Одинцова ул. 61",
                "user_address": "Минск, Одинцова ул. 61",
                "latitude": 53.895447,
                "longitude": 27.443108
            },
            "price": {
                "amount": "81000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "181407.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "81000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 7,
            "number_of_floors": 9,
            "area": {
                "total": 73.3,
                "living": 43.1,
                "kitchen": 9.3
            },
            "photo": "https://static.realt.by/user/vx/u/site14gphuvx/c33dd014f11f3b2b.jpg?1579871656",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-21T18:01:04+0300",
            "last_time_up": "2020-03-05T11:23:59+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/322815",
            "auction_bid": {
                "amount": "2.00",
                "currency": "BYN"
            }
        },
        {
            "id": 336124,
            "author_id": 2837884,
            "location": {
                "address": "Минск, проспект Дзержинского, 76",
                "user_address": "Минск, проспект Дзержинского, 76",
                "latitude": 53.871437,
                "longitude": 27.49011
            },
            "price": {
                "amount": "110000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "246356.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "110000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 8,
            "number_of_floors": 17,
            "area": {
                "total": 62.4,
                "living": 34.5,
                "kitchen": 11.5
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2837884/600x400/216e7729e4efcf7bd7ac8ca49404b225.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-04T09:06:27+0300",
            "last_time_up": "2020-03-04T09:06:27+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/336124",
            "auction_bid": {
                "amount": "2.00",
                "currency": "BYN"
            }
        },
        {
            "id": 334348,
            "author_id": 2837884,
            "location": {
                "address": "Минск, улица Тимошенко, 20 к2",
                "user_address": "Минск, улица Тимошенко, 20 к2",
                "latitude": 53.901421,
                "longitude": 27.459864
            },
            "price": {
                "amount": "88000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "197084.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "88000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 9,
            "number_of_floors": 9,
            "area": {
                "total": 72,
                "living": 43.3,
                "kitchen": 9.3
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2837884/600x400/562c847782860dcf1bcfd40ec7018572.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-28T08:55:27+0300",
            "last_time_up": "2020-03-05T17:56:52+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/334348",
            "auction_bid": {
                "amount": "2.00",
                "currency": "BYN"
            }
        },
        {
            "id": 336689,
            "author_id": 1205479,
            "location": {
                "address": "Минск, улица Пимена Панченко, 80",
                "user_address": "Минск, улица Пимена Панченко 80",
                "latitude": 53.894516,
                "longitude": 27.416567
            },
            "price": {
                "amount": "62000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "138855.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "62000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 4,
            "number_of_floors": 9,
            "area": {
                "total": 52.2,
                "living": 21.9,
                "kitchen": 11
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1205479/600x400/c2997dd7a39e9b38350a6729ec0841f0.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-05T19:51:32+0300",
            "last_time_up": "2020-03-08T20:17:19+0300",
            "up_available_in": 225997,
            "url": "https://r.onliner.by/pk/apartments/336689",
            "auction_bid": {
                "amount": "2.00",
                "currency": "BYN"
            }
        },
        {
            "id": 331754,
            "author_id": 2165460,
            "location": {
                "address": "Минск, улица Розы Люксембург, 143",
                "user_address": "Минск, улица Розы Люксембург, 143",
                "latitude": 53.889034,
                "longitude": 27.51228
            },
            "price": {
                "amount": "137000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "306825.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "137000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 9,
            "number_of_floors": 14,
            "area": {
                "total": 71,
                "living": 40,
                "kitchen": 12
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2165460/600x400/ef1c77df1a3567c9730eb25dae36f153.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-19T23:56:57+0300",
            "last_time_up": "2020-03-06T14:19:14+0300",
            "up_available_in": 31712,
            "url": "https://r.onliner.by/pk/apartments/331754",
            "auction_bid": {
                "amount": "1.50",
                "currency": "BYN"
            }
        },
        {
            "id": 302251,
            "author_id": 1922659,
            "location": {
                "address": "Колодищанский сельский Совет, Колодищи, улица Энтузиастов, 65",
                "user_address": "Колодищанский сельский Совет, Колодищи, улица Энтузиастов",
                "latitude": 53.909878,
                "longitude": 27.773167
            },
            "price": {
                "amount": "55000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "123178.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "55000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 3,
            "floor": 2,
            "number_of_floors": 2,
            "area": {
                "total": 150,
                "living": 77,
                "kitchen": 17
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1922659/600x400/065d7a21463566574f88f19efcc8db29.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-10-24T15:37:25+0300",
            "last_time_up": "2020-03-08T11:46:11+0300",
            "up_available_in": 195329,
            "url": "https://r.onliner.by/pk/apartments/302251",
            "auction_bid": {
                "amount": "1.50",
                "currency": "BYN"
            }
        },
        {
            "id": 287069,
            "author_id": 835539,
            "location": {
                "address": "Минск, улица Голодеда, 23 к1",
                "user_address": "Минск, улица Голодеда, 23 к1",
                "latitude": 53.837929,
                "longitude": 27.634855
            },
            "price": {
                "amount": "73000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "163490.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "73000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 4,
            "number_of_floors": 5,
            "area": {
                "total": 55,
                "living": 36,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/835539/600x400/9da7681e246034416b63db6a656e244b.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-08-18T13:58:40+0300",
            "last_time_up": "2020-03-06T20:23:38+0300",
            "up_available_in": 53576,
            "url": "https://r.onliner.by/pk/apartments/287069",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 250744,
            "author_id": 2180063,
            "location": {
                "address": "Минск, улица Героев 120-й Дивизии",
                "user_address": "Минск, улица Героев 120-й Дивизии д.4",
                "latitude": 53.948082,
                "longitude": 27.707415
            },
            "price": {
                "amount": "96000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "215001.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "96000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 8,
            "number_of_floors": 15,
            "area": {
                "total": 62,
                "living": 41,
                "kitchen": 10
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2180063/600x400/afcd202384ec5e182160e882163346c5.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-02-22T13:09:12+0300",
            "last_time_up": "2020-02-13T11:54:23+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/250744",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 331287,
            "author_id": 264175,
            "location": {
                "address": "Минск, проспект Дзержинского, 22",
                "user_address": "Минск, проспект Дзержинского, 22",
                "latitude": 53.888947,
                "longitude": 27.519064
            },
            "price": {
                "amount": "131000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "293387.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "131000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 13,
            "number_of_floors": 19,
            "area": {
                "total": 99.5,
                "living": 52.1,
                "kitchen": 13.1
            },
            "photo": "https://content.onliner.by/apartment_for_sale/264175/600x400/30efaf45721cad6d964fc75e3d6e8e5f.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-18T15:26:56+0300",
            "last_time_up": "2020-03-06T16:43:01+0300",
            "up_available_in": 40339,
            "url": "https://r.onliner.by/pk/apartments/331287",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 285566,
            "author_id": 389561,
            "location": {
                "address": "Боровлянский сельский Совет, Копище, улица Эрхарт",
                "user_address": "Боровлянский сельский Совет, Копище, улица Эрхарт",
                "latitude": 53.960838,
                "longitude": 27.667637
            },
            "price": {
                "amount": "77000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "172449.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "77000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 8,
            "number_of_floors": 10,
            "area": {
                "total": 42.8,
                "living": 42.8,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/389561/600x400/27cca151580823eb7eae7284895a1b55.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-08-08T14:47:06+0300",
            "last_time_up": "2020-03-05T17:42:30+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/285566",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 325947,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Пионерская ул., 5",
                "user_address": "Пионерская ул., 5",
                "latitude": 53.938702,
                "longitude": 27.489401
            },
            "price": {
                "amount": "189000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "423284.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "189000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 2,
            "number_of_floors": 5,
            "area": {
                "total": 75.7,
                "living": 38.9,
                "kitchen": 12.2
            },
            "photo": "https://manager.t-s.by//uploads/flats/980549/flats/980549-28.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-01T14:02:52+0300",
            "last_time_up": "2020-02-21T16:41:21+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/325947",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 327064,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Якубовского ул., 56",
                "user_address": "Якубовского ул., 56",
                "latitude": 53.892899,
                "longitude": 27.4478
            },
            "price": {
                "amount": "51000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "114219.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "51000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 2,
            "number_of_floors": 9,
            "area": {
                "total": 33.7,
                "living": 16.7,
                "kitchen": 7.1
            },
            "photo": "https://manager.t-s.by//uploads/flats/980589/flats/980589-1.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-05T13:06:17+0300",
            "last_time_up": "2020-03-04T17:29:56+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/327064",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 325021,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Налибокская ул., 34",
                "user_address": "Налибокская ул., 34",
                "latitude": 53.928902,
                "longitude": 27.4335
            },
            "price": {
                "amount": "101000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "226199.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "101000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 10,
            "number_of_floors": 25,
            "area": {
                "total": 62.3,
                "living": 30.3,
                "kitchen": 11
            },
            "photo": "https://manager.t-s.by//uploads/flats/955937/main/img_955937_main.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-29T12:44:39+0300",
            "last_time_up": "2020-02-28T15:08:08+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/325021",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 318904,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Грибоедова ул., 11",
                "user_address": "Грибоедова ул., 11",
                "latitude": 53.914299,
                "longitude": 27.526899
            },
            "price": {
                "amount": "329000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "736828.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "329000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 2,
            "number_of_floors": 10,
            "area": {
                "total": 114.6,
                "living": 76.8,
                "kitchen": 1
            },
            "photo": "https://manager.t-s.by//uploads/flats/980334/flats/980334-SLT-22.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-08T09:37:43+0300",
            "last_time_up": "2020-02-28T17:14:55+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/318904",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 322056,
            "author_id": 1098961,
            "location": {
                "address": "Минск, улица Щорса, 11",
                "user_address": "Минск, улица Щорса, 11",
                "latitude": 53.885666,
                "longitude": 27.514658
            },
            "price": {
                "amount": "190900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "427539.64",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "190900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 7,
            "number_of_floors": 14,
            "area": {
                "total": 95.5,
                "living": 67.5,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1098961/600x400/fd7fa57c88a1b9832012a6e286383f4d.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-19T00:15:33+0300",
            "last_time_up": "2020-03-08T10:46:57+0300",
            "up_available_in": 191775,
            "url": "https://r.onliner.by/pk/apartments/322056",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 321039,
            "author_id": 1098961,
            "location": {
                "address": "Минск, улица Лещинского, 33 к1",
                "user_address": "Минск, улица Лещинского, 33 к1",
                "latitude": 53.912788,
                "longitude": 27.447157
            },
            "price": {
                "amount": "116500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "260913.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "116500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 3,
            "number_of_floors": 9,
            "area": {
                "total": 77.8,
                "living": 45.1,
                "kitchen": 12.7
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1098961/600x400/6e8b4daf8d4d3668faca94fc7e9a6217.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-15T13:03:22+0300",
            "last_time_up": "2020-03-08T10:46:56+0300",
            "up_available_in": 191774,
            "url": "https://r.onliner.by/pk/apartments/321039",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 317513,
            "author_id": 1098961,
            "location": {
                "address": "Минск, Могилёвская улица, 20",
                "user_address": "Минск, Могилёвская улица, 20",
                "latitude": 53.884769,
                "longitude": 27.548607
            },
            "price": {
                "amount": "97000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "217241.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "97000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 3,
            "number_of_floors": 15,
            "area": {
                "total": 64.9,
                "living": 37.7,
                "kitchen": 12.4
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1098961/600x400/aaca70e99372bba23b510ef8bae6e701.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-12-29T14:11:23+0300",
            "last_time_up": "2020-03-08T10:46:56+0300",
            "up_available_in": 191774,
            "url": "https://r.onliner.by/pk/apartments/317513",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 330661,
            "author_id": 1608827,
            "location": {
                "address": "",
                "user_address": "Некрашевича ул., 8",
                "latitude": 53.851074,
                "longitude": 27.493254
            },
            "price": {
                "amount": "85500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "191485.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "85500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 10,
            "number_of_floors": 10,
            "area": {
                "total": 58.3,
                "living": 31.1,
                "kitchen": 12.5
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1608827/600x400/6e0b625a660e2b07d3a5455a62d06f28.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-17T11:55:50+0300",
            "last_time_up": "2020-03-06T13:18:14+0300",
            "up_available_in": 28052,
            "url": "https://r.onliner.by/pk/apartments/330661",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 330457,
            "author_id": 171106,
            "location": {
                "address": "Минск, улица Притыцкого, 97",
                "user_address": "Минск, улица Притыцкого, 97",
                "latitude": 53.90588,
                "longitude": 27.436581
            },
            "price": {
                "amount": "135000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "302346.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "135000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 6,
            "number_of_floors": 9,
            "area": {
                "total": 65.8,
                "living": 23.8,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/171106/600x400/851618335f11e2e94a87d1c7fb219580.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-16T13:07:38+0300",
            "last_time_up": "2020-03-02T17:15:25+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/330457",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 335030,
            "author_id": 1647385,
            "location": {
                "address": "Боровлянский сельский Совет, Копище, улица Братьев Райт, 4",
                "user_address": "Боровлянский сельский Совет, Копище, улица Братьев Райт, 4",
                "latitude": 53.959461,
                "longitude": 27.666716
            },
            "price": {
                "amount": "61900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "138631.24",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "61900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 5,
            "number_of_floors": 8,
            "area": {
                "total": 37.6,
                "living": 18.9,
                "kitchen": 9.4
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1647385/600x400/8d6d13ef02d3887cb5ef61f147c95a0b.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-02T14:20:11+0300",
            "last_time_up": "2020-03-06T08:28:22+0300",
            "up_available_in": 10660,
            "url": "https://r.onliner.by/pk/apartments/335030",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 334637,
            "author_id": 2870346,
            "location": {
                "address": "Минск, улица Алибегова, 12",
                "user_address": "Минск, улица Алибегова, 12",
                "latitude": 53.873417,
                "longitude": 27.483728
            },
            "price": {
                "amount": "226000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "506149.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "226000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 3,
            "number_of_floors": 7,
            "area": {
                "total": 94.5,
                "living": 83.8,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2870346/600x400/3c606f1a24ebd5aff9e2c5d5d77516e2.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-28T17:11:03+0300",
            "last_time_up": "2020-03-06T16:19:17+0300",
            "up_available_in": 38915,
            "url": "https://r.onliner.by/pk/apartments/334637",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 305586,
            "author_id": 1098961,
            "location": {
                "address": "Ждановичи, Славянская улица",
                "user_address": "Ждановичи, Славянская улица",
                "latitude": 53.93993,
                "longitude": 27.411894
            },
            "price": {
                "amount": "229000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "512868.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "229000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 4,
            "floor": 2,
            "number_of_floors": 2,
            "area": {
                "total": 181,
                "living": 82,
                "kitchen": 15.5
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1098961/600x400/78a937d6540574fd73634663523b53b9.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-11-11T02:46:05+0300",
            "last_time_up": "2020-03-08T10:46:55+0300",
            "up_available_in": 191773,
            "url": "https://r.onliner.by/pk/apartments/305586",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 331289,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Гурского ул., 37",
                "user_address": "Гурского ул., 37",
                "latitude": 53.8783,
                "longitude": 27.4923
            },
            "price": {
                "amount": "135000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "302346.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "135000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 4,
            "floor": 8,
            "number_of_floors": 9,
            "area": {
                "total": 87,
                "living": 55.5,
                "kitchen": 10.4
            },
            "photo": "https://manager.t-s.by//uploads/flats/980701/flats/980701-IMG_0678.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-18T15:28:13+0300",
            "last_time_up": "2020-03-03T14:05:50+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/331289",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 330132,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Сурганова ул., 80",
                "user_address": "Сурганова ул., 80",
                "latitude": 53.931099,
                "longitude": 27.5807
            },
            "price": {
                "amount": "74000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "165730.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "74000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 1,
            "number_of_floors": 9,
            "area": {
                "total": 49.7,
                "living": 29.1,
                "kitchen": 8.7
            },
            "photo": "https://manager.t-s.by//uploads/flats/980673/flats/980673-0.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-14T15:40:14+0300",
            "last_time_up": "2020-03-03T15:57:05+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/330132",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 331625,
            "author_id": 2962034,
            "location": {
                "address": "Минск, проспект Дзержинского, 119",
                "user_address": "Минск, проспект Дзержинского, 119",
                "latitude": 53.852184,
                "longitude": 27.477364
            },
            "price": {
                "amount": "143000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "320262.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "143000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 4,
            "floor": 15,
            "number_of_floors": 15,
            "area": {
                "total": 96.7,
                "living": 86.3,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2962034/600x400/1887ef8675f70d3774508f959ba3e679.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-19T15:01:41+0300",
            "last_time_up": "2020-03-06T09:11:54+0300",
            "up_available_in": 13272,
            "url": "https://r.onliner.by/pk/apartments/331625",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 333929,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Коласа ул., 39",
                "user_address": "Коласа ул., 39",
                "latitude": 53.926998,
                "longitude": 27.5945
            },
            "price": {
                "amount": "74000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "165730.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "74000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 4,
            "number_of_floors": 5,
            "area": {
                "total": 52.5,
                "living": 34,
                "kitchen": 6
            },
            "photo": "https://manager.t-s.by//uploads/flats/980398/flats/980398-1.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-26T17:10:55+0300",
            "last_time_up": "2020-03-06T13:19:05+0300",
            "up_available_in": 28103,
            "url": "https://r.onliner.by/pk/apartments/333929",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 329517,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Илимская ул., 29",
                "user_address": "Илимская ул., 29",
                "latitude": 53.890499,
                "longitude": 27.6819
            },
            "price": {
                "amount": "49000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "109740.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "49000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 7,
            "number_of_floors": 9,
            "area": {
                "total": 33.1,
                "living": 17.2,
                "kitchen": 7.2
            },
            "photo": "https://manager.t-s.by//uploads/flats/980655/flats/980655-1.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-13T10:04:36+0300",
            "last_time_up": "2020-03-06T13:19:55+0300",
            "up_available_in": 28153,
            "url": "https://r.onliner.by/pk/apartments/329517",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 330256,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Германовская ул., 17",
                "user_address": "Германовская ул., 17",
                "latitude": 53.869999,
                "longitude": 27.5588
            },
            "price": {
                "amount": "68500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "153412.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "68500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 10,
            "number_of_floors": 10,
            "area": {
                "total": 37.5,
                "living": 17,
                "kitchen": 9.2
            },
            "photo": "https://manager.t-s.by//uploads/flats/980674/flats/980674-3.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-14T17:50:42+0300",
            "last_time_up": "2020-03-06T13:21:20+0300",
            "up_available_in": 28238,
            "url": "https://r.onliner.by/pk/apartments/330256",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 308900,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Победителей просп., 135 ,Б",
                "user_address": "Победителей просп., 135 ,Б",
                "latitude": 53.943199,
                "longitude": 27.4615
            },
            "price": {
                "amount": "163000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "365054.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "163000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 16,
            "number_of_floors": 26,
            "area": {
                "total": 66,
                "living": 61,
                "kitchen": 1
            },
            "photo": "https://manager.t-s.by//uploads/flats/980130/flats/980130-1.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-11-22T14:20:50+0300",
            "last_time_up": "2020-03-06T13:21:44+0300",
            "up_available_in": 28262,
            "url": "https://r.onliner.by/pk/apartments/308900",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 323791,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Стахановская ул., 25",
                "user_address": "Стахановская ул., 25",
                "latitude": 53.886101,
                "longitude": 27.610399
            },
            "price": {
                "amount": "110000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "246356.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "110000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 2,
            "number_of_floors": 3,
            "area": {
                "total": 78.3,
                "living": 54.3,
                "kitchen": 7.7
            },
            "photo": "https://manager.t-s.by//uploads/flats/980361/flats/980361-1.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-24T14:26:52+0300",
            "last_time_up": "2020-03-06T13:47:48+0300",
            "up_available_in": 29826,
            "url": "https://r.onliner.by/pk/apartments/323791",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 297619,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Независимости просп., 95 ,А",
                "user_address": "Независимости просп., 95 ,А",
                "latitude": 53.926201,
                "longitude": 27.618799
            },
            "price": {
                "amount": "368500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "825292.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "368500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 4,
            "floor": 10,
            "number_of_floors": 11,
            "area": {
                "total": 167.4,
                "living": 82.2,
                "kitchen": 21.2
            },
            "photo": "https://manager.t-s.by//uploads/flats/962330/main/img_962330_main.jpg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-10-03T15:48:44+0300",
            "last_time_up": "2020-03-04T12:33:07+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/297619",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 331153,
            "author_id": 1708337,
            "location": {
                "address": "Беларусь, Минск, Голодеда ул., 67",
                "user_address": "Голодеда ул., 67",
                "latitude": 53.839901,
                "longitude": 27.633699
            },
            "price": {
                "amount": "54900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "122954.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "54900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 2,
            "number_of_floors": 5,
            "area": {
                "total": 49.8,
                "living": 34.6,
                "kitchen": 6.2
            },
            "photo": "https://manager.t-s.by//uploads/flats/980697/flats/980697-0.JPG",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-18T11:27:37+0300",
            "last_time_up": "2020-03-06T13:49:11+0300",
            "up_available_in": 29909,
            "url": "https://r.onliner.by/pk/apartments/331153",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 331433,
            "author_id": 261322,
            "location": {
                "address": "Минск, улица Солтыса, 36",
                "user_address": "Минск, улица Солтыса, 36",
                "latitude": 53.892742,
                "longitude": 27.658913
            },
            "price": {
                "amount": "53800.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "120490.48",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "53800.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 1,
            "floor": 6,
            "number_of_floors": 10,
            "area": {
                "total": 37.9,
                "living": 17.9,
                "kitchen": 9
            },
            "photo": "https://content.onliner.by/apartment_for_sale/261322/600x400/11e2f99f67b90eea971055433e31cfaa.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-19T09:44:55+0300",
            "last_time_up": "2020-03-07T13:42:03+0300",
            "up_available_in": 115881,
            "url": "https://r.onliner.by/pk/apartments/331433",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 332110,
            "author_id": 2837884,
            "location": {
                "address": "Минск, улица Скрипникова, 32",
                "user_address": "Минск, улица Скрипникова, 32",
                "latitude": 53.89769,
                "longitude": 27.423706
            },
            "price": {
                "amount": "63700.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "142662.52",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "63700.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 2,
            "number_of_floors": 9,
            "area": {
                "total": 52.1,
                "living": 17.9,
                "kitchen": 12
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2837884/600x400/ebcd299ccc12fbde5a47865f3864619f.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-02-20T19:45:08+0300",
            "last_time_up": "2020-03-05T17:56:53+0300",
            "up_available_in": 0,
            "url": "https://r.onliner.by/pk/apartments/332110",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 323705,
            "author_id": 2447025,
            "location": {
                "address": "Колодищанский сельский Совет, Колодищи, Рождественская улица, 10-12",
                "user_address": "Колодищанский сельский Совет, Колодищи, улица Энтузиастов",
                "latitude": 53.910606,
                "longitude": 27.77474
            },
            "price": {
                "amount": "58500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "131016.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "58500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 3,
            "floor": 1,
            "number_of_floors": 2,
            "area": {
                "total": 125,
                "living": 71,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2447025/600x400/623fd86d07cadffd2663f977d8203516.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-01-24T11:12:44+0300",
            "last_time_up": "2020-03-08T11:37:12+0300",
            "up_available_in": 194790,
            "url": "https://r.onliner.by/pk/apartments/323705",
            "auction_bid": {
                "amount": "1.00",
                "currency": "BYN"
            }
        },
        {
            "id": 337215,
            "author_id": 2810684,
            "location": {
                "address": "Минск, проспект Независимости, 164",
                "user_address": "Минск, проспект Независимости, 164",
                "latitude": 53.943855,
                "longitude": 27.686243
            },
            "price": {
                "amount": "89000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "199324.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "89000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 2,
            "number_of_floors": 19,
            "area": {
                "total": 63.3,
                "living": 39,
                "kitchen": 10.4
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2810684/600x400/7d9b336e7e17e01530ebe49adab578f4.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-09T01:40:29+0300",
            "last_time_up": "2020-03-09T01:40:29+0300",
            "up_available_in": 245387,
            "url": "https://r.onliner.by/pk/apartments/337215",
            "auction_bid": null
        },
        {
            "id": 180494,
            "author_id": 326726,
            "location": {
                "address": "Минск, улица Киселёва, 10",
                "user_address": "Минск, Киселева, 10",
                "latitude": 53.911285,
                "longitude": 27.571924
            },
            "price": {
                "amount": "149000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "333700.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "149000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 4,
            "number_of_floors": 5,
            "area": {
                "total": 41.3,
                "living": 29.9,
                "kitchen": 11
            },
            "photo": "https://content.onliner.by/apartment_for_sale/326726/600x400/990e43f9a0953242d8da7169e814968a.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2018-04-10T15:39:27+0300",
            "last_time_up": "2020-03-09T00:35:51+0300",
            "up_available_in": 241509,
            "url": "https://r.onliner.by/pk/apartments/180494",
            "auction_bid": null
        },
        {
            "id": 336721,
            "author_id": 2974033,
            "location": {
                "address": "Минск, улица Громова, 46",
                "user_address": "Минск, улица Громова, 46",
                "latitude": 53.851234,
                "longitude": 27.438526
            },
            "price": {
                "amount": "79000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "176928.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "79000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 6,
            "number_of_floors": 9,
            "area": {
                "total": 49,
                "living": 28.4,
                "kitchen": 7.2
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2974033/600x400/638bda70bea52c95cee209edb8985f24.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-05T22:49:11+0300",
            "last_time_up": "2020-03-09T00:28:42+0300",
            "up_available_in": 241080,
            "url": "https://r.onliner.by/pk/apartments/336721",
            "auction_bid": null
        },
        {
            "id": 329424,
            "author_id": 2837349,
            "location": {
                "address": "Minsk, улица Макаёнка, 12В",
                "user_address": "Минск, улица Макаёнка, 12В",
                "latitude": 53.92384,
                "longitude": 27.62582
            },
            "price": {
                "amount": "69900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "156548.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "69900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 1,
            "number_of_floors": 19,
            "area": {
                "total": 45.4,
                "living": 35.1,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2837349/600x400/c2eb08b4917fc9bf189896aac2fc8fc4.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-12T23:45:55+0300",
            "last_time_up": "2020-03-09T00:21:46+0300",
            "up_available_in": 240664,
            "url": "https://r.onliner.by/pk/apartments/329424",
            "auction_bid": null
        },
        {
            "id": 325557,
            "author_id": 2405424,
            "location": {
                "address": "Минск, улица Седых, 62",
                "user_address": "Минск, улица Седых, 62",
                "latitude": 53.947155,
                "longitude": 27.637983
            },
            "price": {
                "amount": "69000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "154532.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "69000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 4,
            "number_of_floors": 5,
            "area": {
                "total": 38.6,
                "living": 23.9,
                "kitchen": 6.1
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2405424/600x400/2ceee3369d68ce5b8a84c150d3f6d69f.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-01-30T21:11:58+0300",
            "last_time_up": "2020-03-09T00:07:39+0300",
            "up_available_in": 239817,
            "url": "https://r.onliner.by/pk/apartments/325557",
            "auction_bid": null
        },
        {
            "id": 266326,
            "author_id": 827743,
            "location": {
                "address": "Минск, Мястровская улица, 6",
                "user_address": "Минск, Мястровская улица, 6",
                "latitude": 53.93354,
                "longitude": 27.475601
            },
            "price": {
                "amount": "86400.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "193501.44",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "86400.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 14,
            "number_of_floors": 22,
            "area": {
                "total": 45.5,
                "living": 40,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/827743/600x400/a2486d4b84e86adabaf7beff25b850be.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-04-16T11:13:15+0300",
            "last_time_up": "2020-03-08T23:43:54+0300",
            "up_available_in": 238392,
            "url": "https://r.onliner.by/pk/apartments/266326",
            "auction_bid": null
        },
        {
            "id": 334994,
            "author_id": 827743,
            "location": {
                "address": "Ждановичский сельский Совет, Ратомка, улица Солнечный Пляж",
                "user_address": "Ждановичский сельский Совет, Ратомка, улица Солнечный Пляж",
                "latitude": 53.946766,
                "longitude": 27.330603
            },
            "price": {
                "amount": "147000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "329221.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "147000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 3,
            "floor": 1,
            "number_of_floors": 5,
            "area": {
                "total": 90,
                "living": 68,
                "kitchen": null
            },
            "photo": "https://content.onliner.by/apartment_for_sale/827743/600x400/35e785d87471ecf8dcb201e480f28f1f.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-02T12:24:09+0300",
            "last_time_up": "2020-03-08T23:43:50+0300",
            "up_available_in": 238388,
            "url": "https://r.onliner.by/pk/apartments/334994",
            "auction_bid": null
        },
        {
            "id": 298738,
            "author_id": 2671542,
            "location": {
                "address": "Минск, улица Рафиева, 85",
                "user_address": "Минск, улица Рафиева, 85",
                "latitude": 53.860458,
                "longitude": 27.439402
            },
            "price": {
                "amount": "65000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "145574.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "65000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 8,
            "number_of_floors": 9,
            "area": {
                "total": 51,
                "living": 30,
                "kitchen": 9
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2671542/600x400/59ed732fe2ed177fffd332bdd55cbcb9.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-10-09T13:17:00+0300",
            "last_time_up": "2020-03-08T23:26:56+0300",
            "up_available_in": 237374,
            "url": "https://r.onliner.by/pk/apartments/298738",
            "auction_bid": null
        },
        {
            "id": 337210,
            "author_id": 423285,
            "location": {
                "address": "Минск, проспект Победителей, 125",
                "user_address": "Минск, проспект Победителей, 125",
                "latitude": 53.939693,
                "longitude": 27.469057
            },
            "price": {
                "amount": "120000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "268752.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "120000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 9,
            "number_of_floors": 12,
            "area": {
                "total": 54.3,
                "living": 29.8,
                "kitchen": 9.3
            },
            "photo": "https://content.onliner.by/apartment_for_sale/423285/600x400/ac820845593e6bd2a7cd67420b60188a.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-08T22:40:18+0300",
            "last_time_up": "2020-03-08T22:40:18+0300",
            "up_available_in": 234576,
            "url": "https://r.onliner.by/pk/apartments/337210",
            "auction_bid": null
        },
        {
            "id": 319508,
            "author_id": 249568,
            "location": {
                "address": "Минск, улица Лобанка, 88",
                "user_address": "Минск, улица Лобанка, 88",
                "latitude": 53.890221,
                "longitude": 27.427206
            },
            "price": {
                "amount": "106660.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "238875.74",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "106660.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 6,
            "number_of_floors": 9,
            "area": {
                "total": 79,
                "living": 42,
                "kitchen": 13.5
            },
            "photo": "https://content.onliner.by/apartment_for_sale/249568/600x400/d761c430e86017dd78d3357db4f3ce9e.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-01-09T18:03:03+0300",
            "last_time_up": "2020-03-08T22:23:23+0300",
            "up_available_in": 233561,
            "url": "https://r.onliner.by/pk/apartments/319508",
            "auction_bid": null
        },
        {
            "id": 328168,
            "author_id": 682786,
            "location": {
                "address": "Минск, улица Нестерова, 55",
                "user_address": "Минск, улица Нестерова, 55",
                "latitude": 53.886406,
                "longitude": 27.68552
            },
            "price": {
                "amount": "86000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "192605.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "86000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 6,
            "number_of_floors": 9,
            "area": {
                "total": 63,
                "living": 43.1,
                "kitchen": 7.2
            },
            "photo": "https://content.onliner.by/apartment_for_sale/682786/600x400/7b1ce69e8adcbaa60a4cd8d8649cc228.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-08T19:48:09+0300",
            "last_time_up": "2020-03-08T22:18:55+0300",
            "up_available_in": 233293,
            "url": "https://r.onliner.by/pk/apartments/328168",
            "auction_bid": null
        },
        {
            "id": 337206,
            "author_id": 952614,
            "location": {
                "address": "Минск, улица Челюскинцев, 73",
                "user_address": "Минск, улица Челюскинцев, 73",
                "latitude": 53.874264,
                "longitude": 27.653299
            },
            "price": {
                "amount": "57000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "127657.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "57000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 1,
            "number_of_floors": 2,
            "area": {
                "total": 40.1,
                "living": 24.4,
                "kitchen": 5.6
            },
            "photo": "https://content.onliner.by/apartment_for_sale/952614/600x400/fceee134bb09028fda7d6090355d52df.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-08T21:58:54+0300",
            "last_time_up": "2020-03-08T21:58:54+0300",
            "up_available_in": 232092,
            "url": "https://r.onliner.by/pk/apartments/337206",
            "auction_bid": null
        },
        {
            "id": 320151,
            "author_id": 1719445,
            "location": {
                "address": "Минск, улица Маршала Лосика, 16",
                "user_address": "Минск, улица Маршала Лосика",
                "latitude": 53.873436,
                "longitude": 27.447952
            },
            "price": {
                "amount": "89299.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "199994.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "89299.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 14,
            "number_of_floors": 16,
            "area": {
                "total": 64.5,
                "living": 35.6,
                "kitchen": 12.7
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1719445/600x400/077b97def994f41a77cdfe9b6b6fe40e.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-01-12T23:03:38+0300",
            "last_time_up": "2020-03-08T21:51:31+0300",
            "up_available_in": 231649,
            "url": "https://r.onliner.by/pk/apartments/320151",
            "auction_bid": null
        },
        {
            "id": 326672,
            "author_id": 502968,
            "location": {
                "address": "Ждановичский сельский Совет, Ратомка, улица Солнечный Пляж, 3",
                "user_address": "Ждановичский сельский Совет, Ратомка, улица Солнечный Пляж",
                "latitude": 53.947449,
                "longitude": 27.330681
            },
            "price": {
                "amount": "340000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "761464.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "340000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 5,
            "floor": 2,
            "number_of_floors": 2,
            "area": {
                "total": 140,
                "living": 129,
                "kitchen": 10
            },
            "photo": "https://content.onliner.by/apartment_for_sale/502968/600x400/426e02c89ca3148bd93646bc321022fb.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-04T12:33:25+0300",
            "last_time_up": "2020-03-08T21:49:09+0300",
            "up_available_in": 231507,
            "url": "https://r.onliner.by/pk/apartments/326672",
            "auction_bid": null
        },
        {
            "id": 318736,
            "author_id": 1976739,
            "location": {
                "address": "Минск, улица Якубова, 32",
                "user_address": "Минск, улица Якубова, 32",
                "latitude": 53.854706,
                "longitude": 27.596165
            },
            "price": {
                "amount": "76400.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "171105.44",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "76400.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 5,
            "number_of_floors": 9,
            "area": {
                "total": 52.5,
                "living": 29.5,
                "kitchen": 8.8
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1976739/600x400/f55881dbc53f3b1e185e7cf25139571e.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-01-06T15:42:00+0300",
            "last_time_up": "2020-03-08T21:42:00+0300",
            "up_available_in": 231078,
            "url": "https://r.onliner.by/pk/apartments/318736",
            "auction_bid": null
        },
        {
            "id": 302522,
            "author_id": 1869120,
            "location": {
                "address": "Минск, Сухаревская ул. 39",
                "user_address": "Минск, Сухаревская ул. 39",
                "latitude": 53.886501,
                "longitude": 27.421
            },
            "price": {
                "amount": "94000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "210522.40",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "94000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 4,
            "floor": 9,
            "number_of_floors": 9,
            "area": {
                "total": 76,
                "living": 49,
                "kitchen": 9
            },
            "photo": "https://static.realt.by/user/i8/0/site1zx4f0i8/20a2b4be71838730.jpg?1572006360",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-10-25T17:01:03+0300",
            "last_time_up": "2020-03-08T21:31:03+0300",
            "up_available_in": 230421,
            "url": "https://r.onliner.by/pk/apartments/302522",
            "auction_bid": null
        },
        {
            "id": 337205,
            "author_id": 1647385,
            "location": {
                "address": "Минск, Копище, улица Лопатина, 5",
                "user_address": "Минск, Копище, улица Лопатина, 5",
                "latitude": 53.955341,
                "longitude": 27.676218
            },
            "price": {
                "amount": "139900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "313320.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "139900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 21,
            "number_of_floors": 23,
            "area": {
                "total": 64,
                "living": 54,
                "kitchen": 10
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1647385/600x400/a7a3d3c09ce088dc20a9876b02a4b7e0.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-08T21:27:15+0300",
            "last_time_up": "2020-03-08T21:27:15+0300",
            "up_available_in": 230193,
            "url": "https://r.onliner.by/pk/apartments/337205",
            "auction_bid": null
        },
        {
            "id": 300159,
            "author_id": 2005387,
            "location": {
                "address": "Минск, улица Петра Мстиславца",
                "user_address": "Минск, улица Петра Мстиславца",
                "latitude": 53.933792,
                "longitude": 27.655123
            },
            "price": {
                "amount": "75360.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "168776.26",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "75360.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 12,
            "number_of_floors": 25,
            "area": {
                "total": 47.9,
                "living": 20,
                "kitchen": 12
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/0488b38a0d62f70a47e5b5f7a685b9d6.png",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-10-15T15:41:44+0300",
            "last_time_up": "2020-03-08T21:22:18+0300",
            "up_available_in": 229896,
            "url": "https://r.onliner.by/pk/apartments/300159",
            "auction_bid": null
        },
        {
            "id": 301100,
            "author_id": 2005387,
            "location": {
                "address": "Минск, Братская улица",
                "user_address": "Минск, Братская улица",
                "latitude": 53.867962,
                "longitude": 27.552347
            },
            "price": {
                "amount": "96117.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "215263.63",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "96117.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 3,
            "floor": 6,
            "number_of_floors": 14,
            "area": {
                "total": 71.8,
                "living": 50,
                "kitchen": 15
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/3db46d1075b0bf5124f705bea528fa32.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-10-20T15:03:01+0300",
            "last_time_up": "2020-03-08T21:22:17+0300",
            "up_available_in": 229895,
            "url": "https://r.onliner.by/pk/apartments/301100",
            "auction_bid": null
        },
        {
            "id": 302055,
            "author_id": 2005387,
            "location": {
                "address": "Минск, проспект Независимости",
                "user_address": "Минск, проспект Независимости",
                "latitude": 53.943935,
                "longitude": 27.683002
            },
            "price": {
                "amount": "77920.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "174509.63",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "77920.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 7,
            "number_of_floors": 9,
            "area": {
                "total": 46.7,
                "living": 20,
                "kitchen": 12
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/8352512fae25b5683b284fbc82741803.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-10-24T09:42:11+0300",
            "last_time_up": "2020-03-08T21:22:15+0300",
            "up_available_in": 229893,
            "url": "https://r.onliner.by/pk/apartments/302055",
            "auction_bid": null
        },
        {
            "id": 304681,
            "author_id": 2005387,
            "location": {
                "address": "Минск, улица Петра Мстиславца",
                "user_address": "Минск, улица Петра Мстиславца",
                "latitude": 53.933792,
                "longitude": 27.655123
            },
            "price": {
                "amount": "120940.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "270857.22",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "120940.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 3,
            "floor": 5,
            "number_of_floors": 24,
            "area": {
                "total": 80.6,
                "living": 59,
                "kitchen": 13
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/b179ffb0c177c98348b26112dadddfbf.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-11-05T14:25:31+0300",
            "last_time_up": "2020-03-08T21:22:14+0300",
            "up_available_in": 229892,
            "url": "https://r.onliner.by/pk/apartments/304681",
            "auction_bid": null
        },
        {
            "id": 306340,
            "author_id": 2005387,
            "location": {
                "address": "Минск, проспект Независимости, 39",
                "user_address": "Минск, проспект Независимости, 39",
                "latitude": 53.909973,
                "longitude": 27.576389
            },
            "price": {
                "amount": "128000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "286668.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "128000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 2,
            "number_of_floors": 5,
            "area": {
                "total": 52,
                "living": 34,
                "kitchen": 9
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/644fe1ba7a5927d5c02a069f8c3f4a77.png",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-11-12T16:31:28+0300",
            "last_time_up": "2020-03-08T21:22:11+0300",
            "up_available_in": 229889,
            "url": "https://r.onliner.by/pk/apartments/306340",
            "auction_bid": null
        },
        {
            "id": 309136,
            "author_id": 2005387,
            "location": {
                "address": "Сеницкий сельский Совет",
                "user_address": "Сеницкий сельский Совет",
                "latitude": 53.829601,
                "longitude": 27.55629
            },
            "price": {
                "amount": "29900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "66964.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "29900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 1,
            "number_of_floors": 1,
            "area": {
                "total": 28,
                "living": 16,
                "kitchen": 9
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/907b30703c9e893ecb621be3a17bf948.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-11-24T15:00:33+0300",
            "last_time_up": "2020-03-08T21:22:08+0300",
            "up_available_in": 229886,
            "url": "https://r.onliner.by/pk/apartments/309136",
            "auction_bid": null
        },
        {
            "id": 311218,
            "author_id": 2005387,
            "location": {
                "address": "Минск, Поставская улица",
                "user_address": "Минск, Поставская улица",
                "latitude": 53.954037,
                "longitude": 27.588953
            },
            "price": {
                "amount": "1200000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "2687520.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "1200000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 6,
            "floor": 1,
            "number_of_floors": 2,
            "area": {
                "total": 389,
                "living": 172,
                "kitchen": 20
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/87f96746d1f927663af701d5654f9bfc.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-12-02T19:10:45+0300",
            "last_time_up": "2020-03-08T21:22:08+0300",
            "up_available_in": 229886,
            "url": "https://r.onliner.by/pk/apartments/311218",
            "auction_bid": null
        },
        {
            "id": 314705,
            "author_id": 2005387,
            "location": {
                "address": "Минск, улица Лейтенанта Кижеватова",
                "user_address": "Минск, улица Лейтенанта Кижеватова",
                "latitude": 53.858906,
                "longitude": 27.538528
            },
            "price": {
                "amount": "60380.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "135227.05",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "60380.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 5,
            "number_of_floors": 16,
            "area": {
                "total": 45.6,
                "living": 20,
                "kitchen": 12
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/7724592c69f08dc07acca7a34a414bb0.png",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-12-16T18:25:15+0300",
            "last_time_up": "2020-03-08T21:22:05+0300",
            "up_available_in": 229883,
            "url": "https://r.onliner.by/pk/apartments/314705",
            "auction_bid": null
        },
        {
            "id": 315383,
            "author_id": 2005387,
            "location": {
                "address": "Минск, улица Франциска Скорины, 5",
                "user_address": "Минск, улица Франциска Скорины",
                "latitude": 53.927738,
                "longitude": 27.651644
            },
            "price": {
                "amount": "81970.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "183580.01",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "81970.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 4,
            "number_of_floors": 24,
            "area": {
                "total": 47.5,
                "living": 20,
                "kitchen": 12
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/b179ffb0c177c98348b26112dadddfbf.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2019-12-19T09:33:58+0300",
            "last_time_up": "2020-03-08T21:22:04+0300",
            "up_available_in": 229882,
            "url": "https://r.onliner.by/pk/apartments/315383",
            "auction_bid": null
        },
        {
            "id": 323414,
            "author_id": 2005387,
            "location": {
                "address": "Колодищанский сельский Совет, Колодищи, Мирная улица",
                "user_address": "Колодищанский сельский Совет, Колодищи, Мирная улица",
                "latitude": 53.927471,
                "longitude": 27.755457
            },
            "price": {
                "amount": "360000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "806256.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "360000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 6,
            "floor": 1,
            "number_of_floors": 2,
            "area": {
                "total": 520,
                "living": 360,
                "kitchen": 25
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/52c13d6fc951d85ee57359bd972a3b25.png",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-01-23T13:28:19+0300",
            "last_time_up": "2020-03-08T21:22:03+0300",
            "up_available_in": 229881,
            "url": "https://r.onliner.by/pk/apartments/323414",
            "auction_bid": null
        },
        {
            "id": 329017,
            "author_id": 2005387,
            "location": {
                "address": "Ратомка, Минский район, Минская область, 223035, Беларусь",
                "user_address": "Ратомка, Минский район, Минская область, 223035, Беларусь",
                "latitude": 53.933697,
                "longitude": 27.279654
            },
            "price": {
                "amount": "16000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "35833.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "16000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 1,
            "number_of_floors": 1,
            "area": {
                "total": 50,
                "living": 32,
                "kitchen": 9
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/796ba73f34e281bcc78d59734de9af38.png",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-11T16:36:00+0300",
            "last_time_up": "2020-03-08T21:22:00+0300",
            "up_available_in": 229878,
            "url": "https://r.onliner.by/pk/apartments/329017",
            "auction_bid": null
        },
        {
            "id": 332499,
            "author_id": 2005387,
            "location": {
                "address": "Минск, улица Петра Мстиславца, 12",
                "user_address": "Минск, улица Петра Мстиславца, 12",
                "latitude": 53.931877,
                "longitude": 27.649876
            },
            "price": {
                "amount": "96000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "215001.60",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "96000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 2,
            "floor": 7,
            "number_of_floors": 10,
            "area": {
                "total": 45.4,
                "living": 20,
                "kitchen": 12
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/b1ff8bbbdbeedbdedc0681a2a524da35.png",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-02-21T21:09:29+0300",
            "last_time_up": "2020-03-08T21:21:59+0300",
            "up_available_in": 229877,
            "url": "https://r.onliner.by/pk/apartments/332499",
            "auction_bid": null
        },
        {
            "id": 336531,
            "author_id": 2005387,
            "location": {
                "address": "Минск, улица Петра Мстиславца, 17",
                "user_address": "Минск, улица Петра Мстиславца, 17",
                "latitude": 53.934799,
                "longitude": 27.655336
            },
            "price": {
                "amount": "152000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "340419.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "152000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 3,
            "floor": 7,
            "number_of_floors": 25,
            "area": {
                "total": 83,
                "living": 47,
                "kitchen": 15
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2005387/600x400/f5ca43ecff131ce76e0bea5cffea483b.png",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-05T13:15:20+0300",
            "last_time_up": "2020-03-08T21:21:58+0300",
            "up_available_in": 229876,
            "url": "https://r.onliner.by/pk/apartments/336531",
            "auction_bid": null
        },
        {
            "id": 337202,
            "author_id": 1647385,
            "location": {
                "address": "Минск, улица Асаналиева, 9",
                "user_address": "Минск, улица Асаналиева, 9",
                "latitude": 53.849335,
                "longitude": 27.545599
            },
            "price": {
                "amount": "79900.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "178944.04",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "79900.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 6,
            "number_of_floors": 9,
            "area": {
                "total": 65.4,
                "living": 43.5,
                "kitchen": 7.1
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1647385/600x400/cc379c8786f28381fefa17e78ed63258.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-03-08T20:49:38+0300",
            "last_time_up": "2020-03-08T20:49:38+0300",
            "up_available_in": 227936,
            "url": "https://r.onliner.by/pk/apartments/337202",
            "auction_bid": null
        },
        {
            "id": 337199,
            "author_id": 2976360,
            "location": {
                "address": "Минск, проспект Газеты Правда, 54",
                "user_address": "Минск, проспект Газеты Правда, 54",
                "latitude": 53.860767,
                "longitude": 27.473406
            },
            "price": {
                "amount": "67000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "150053.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "67000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 1,
            "floor": 2,
            "number_of_floors": 9,
            "area": {
                "total": 35,
                "living": 17,
                "kitchen": 7.2
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2976360/600x400/b52ef8f291148b8a99bc82df7972abf2.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-08T20:39:14+0300",
            "last_time_up": "2020-03-08T20:39:14+0300",
            "up_available_in": 227312,
            "url": "https://r.onliner.by/pk/apartments/337199",
            "auction_bid": null
        },
        {
            "id": 336099,
            "author_id": 1290547,
            "location": {
                "address": "Минск, улица Притыцкого, 45",
                "user_address": "Минск, улица Притыцкого, 45",
                "latitude": 53.906124,
                "longitude": 27.476404
            },
            "price": {
                "amount": "220000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "492712.00",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "220000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 4,
            "floor": 2,
            "number_of_floors": 9,
            "area": {
                "total": 125.6,
                "living": 76.5,
                "kitchen": 18.8
            },
            "photo": "https://content.onliner.by/apartment_for_sale/1290547/600x400/4d32b9851c3d92d4fdd49b2480e3bf5f.jpeg",
            "seller": {
                "type": "owner"
            },
            "created_at": "2020-03-03T20:33:31+0300",
            "last_time_up": "2020-03-08T20:29:13+0300",
            "up_available_in": 226711,
            "url": "https://r.onliner.by/pk/apartments/336099",
            "auction_bid": null
        },
        {
            "id": 308240,
            "author_id": 2707185,
            "location": {
                "address": "Минск, Слободской проезд, 22",
                "user_address": "Минск, Слободской проезд, д.22",
                "latitude": 53.850945,
                "longitude": 27.448622
            },
            "price": {
                "amount": "79500.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "178048.20",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "79500.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 3,
            "floor": 4,
            "number_of_floors": 9,
            "area": {
                "total": 63,
                "living": 43,
                "kitchen": 7.2
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2707185/600x400/667db7f6a147a8a4d8ae8fed40bcf955.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2019-11-19T19:47:06+0300",
            "last_time_up": "2020-03-08T20:06:12+0300",
            "up_available_in": 225330,
            "url": "https://r.onliner.by/pk/apartments/308240",
            "auction_bid": null
        },
        {
            "id": 324804,
            "author_id": 2707185,
            "location": {
                "address": "Минск, улица Жуковского",
                "user_address": "Минск, улица Жуковского",
                "latitude": 53.885021,
                "longitude": 27.55102
            },
            "price": {
                "amount": "78000.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "174688.80",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "78000.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": true,
            "number_of_rooms": 2,
            "floor": 6,
            "number_of_floors": 9,
            "area": {
                "total": 48.7,
                "living": 28.4,
                "kitchen": 7.1
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2707185/600x400/05add4443dc4bf369b7497ee7745ee39.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-28T16:50:02+0300",
            "last_time_up": "2020-03-08T20:06:09+0300",
            "up_available_in": 225327,
            "url": "https://r.onliner.by/pk/apartments/324804",
            "auction_bid": null
        },
        {
            "id": 325868,
            "author_id": 2707185,
            "location": {
                "address": "Боровлянский сельский Совет, Копище, Подгорная улица, 19А к2",
                "user_address": "Минск, Копище, улица Подгорная",
                "latitude": 53.961716,
                "longitude": 27.675022
            },
            "price": {
                "amount": "65200.00",
                "currency": "USD",
                "converted": {
                    "BYN": {
                        "amount": "146021.92",
                        "currency": "BYN"
                    },
                    "USD": {
                        "amount": "65200.00",
                        "currency": "USD"
                    }
                }
            },
            "resale": false,
            "number_of_rooms": 1,
            "floor": 5,
            "number_of_floors": 11,
            "area": {
                "total": 41.2,
                "living": 16.1,
                "kitchen": 10.5
            },
            "photo": "https://content.onliner.by/apartment_for_sale/2707185/600x400/756d57e4b58b14cf7432ed08b6beecea.jpeg",
            "seller": {
                "type": "agent"
            },
            "created_at": "2020-01-31T18:34:01+0300",
            "last_time_up": "2020-03-08T20:06:07+0300",
            "up_available_in": 225325,
            "url": "https://r.onliner.by/pk/apartments/325868",
            "auction_bid": null
        }
    ],
    "total": 6184,
    "page": {
        "limit": 96,
        "items": 96,
        "current": 1,
        "last": 8
    }
}
"""


def test_model_creation():
    import json
    data = json.loads(api_response)
    for a in data["apartments"]:
        model = onliner.create_selling_apartment(a)
        assert model.area.living is not None
