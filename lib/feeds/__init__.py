from .onliner import OnlinerPuller
# from .neagent import NeagentPoller


def create(config, storage):
    for c in [OnlinerPuller]:
        if config['name'].find(c.URL):
            return c(config, storage)
