from os import path
import requests
import logging
import copy
import time
import re
import os
import binascii
import json
import dateutil.parser
from ..objects import Apartment, Type, Location
from scrapy.selector import Selector


def create_leasing_apartment(data):
    obj = Apartment()
    obj.price = float(data['price']['converted']['USD']['amount'])
    obj.currency = 'USD'
    obj.address = data['location']['address']
    obj.photos = [data['photo']]
    obj.location = Location(
        address=data['location']['address'],
        latitude=float(data['location']['latitude']),
        longitude=float(data['location']['longitude']))

    m = re.match(r'(\d)_room', data['rent_type'])
    if m:
        obj.rooms = int(m.group(1))
        obj.type = Type.RENT_FLAT
    else:
        obj.rooms = 1
        obj.type = Type.RENT_ROOM
    obj.link = data['url']
    obj.ts = dateutil.parser.parse(data['created_at']).timestamp()
    obj.id = "onliner" + str(data["id"])

    return obj


def create_selling_apartment(data):
    obj = Apartment()
    obj.price = float(data['price']['converted']['USD']['amount'])
    obj.currency = 'USD'
    obj.address = data['location']['address']
    obj.photos = [data['photo']]
    obj.location = Location(
        address=data['location']['address'],
        latitude=float(data['location']['latitude']),
        longitude=float(data['location']['longitude']))

    if 'area' in data:
        obj.area.living = data['area'].get('living', None)
        obj.area.kitchen = data['area'].get('kitchen', None)
        obj.area.total = data['area'].get('total', None)

    obj.rooms = int(data['number_of_rooms'])
    obj.type = Type.SELL
    obj.floor = int(data['number_of_floors'])
    obj.link = data['url']
    obj.ts = dateutil.parser.parse(data['created_at']).timestamp()
    obj.id = "onliner" + str(data["id"])

    return obj


def create_apartment(data, ad_type):
    if ad_type == Type.SELL:
        # if data['seller']['type'] == 'owner':
        return create_selling_apartment(data)
        # else:
        #     logging.debug('apartment %s is being sold by an estate agent', data)
        #     return None
    else:
        return create_leasing_apartment(data)

class OnlinerPuller(object):
    URL = "onliner.by"

    def __init__(self, config, storage):
        self.config = config
        self.storage = storage
        super(OnlinerPuller, self).__init__()

    @property
    def request_config(self):
        return copy.deepcopy(self.config['request'])

    @property
    def api_url(self):
        return self.config['api_url']

    @staticmethod
    def get_phones(html):
        selector = Selector(text=html)
        phone = selector.css('.apartment-info__item_secondary a::text').get()
        return [re.sub(r"[ -]", "", phone)]

    @staticmethod
    def fetch_extra_information(url, apartment):
        logging.info('fetching extra information from %s', url)
        res = requests.get(url)
        if res.status_code == 200:
            apartment.phones = OnlinerPuller.get_phones(res.text)

    def pull(self, on_new_apartment, max_requests=None):
        params = self.request_config
        params['page'] = 1

        requests_count = 0
        metadata = self.storage.get(self.config['name']) or {'ts': 0}
        max_ts = metadata['ts']
        min_ts = max(int(time.time() - 7 * 24 * 60 * 60), metadata['ts'])

        try:
            while True:
                r = requests.get(self.api_url, params)
                requests_count += 1

                if r.status_code == 200:
                    for a in r.json()['apartments']:
                        apartment = create_apartment(a, self.config["type"])

                        if apartment:
                            max_ts = max(max_ts, apartment.ts)
                            if apartment.ts <= min_ts:
                                logging.debug('reached last ad')
                                return

                            self.fetch_extra_information(a['url'], apartment)
                            logging.debug('processed %r', a)
                            on_new_apartment(apartment)
                else:
                    logging.warning('invalid response code=%d', r.status_code)
                    break
                if max_requests is not None and requests_count >= max_requests:
                    break

                params['page'] += 1
        finally:
            self.storage.set(self.config['name'], {'ts': max_ts})
