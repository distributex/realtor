import json
import os
import time
import urllib.parse as urlparse
import logging
import dataclasses
import lxml.html
import requests
from requests.exceptions import HTTPError

# from ..objects import Apartment, Type, Location

@dataclasses.dataclass
class Apartment(object):
    """
    Mock apartment data model
    """
    price: float = 0
    currency: str = "USD"
    link: str = None
    photos: list = ()
    location: str = None


class Neagent(object):
    url = 'http://neagent.by/board/?hasPhotos=1&catid=1'
    PAGE_SIZE = 13
    FETCH_DELAY = 1

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def find(self):
        page_num = 0
        self.logger.info("parsing: {}".format(page_num))
        try:
            while True:
                for apartment in self.parse_page(page_num):
                    yield apartment
                page_num += 1
                time.sleep(self.FETCH_DELAY)
                self.logger.info("parsing: {}".format(page_num))
        except HTTPError as ex:
            raise StopIteration()

    def parse_page(self, page_num):
        resp = requests.get(self.get_url(page_num * self.PAGE_SIZE))
        resp.raise_for_status()
        page = lxml.html.fromstring(resp.text)

        ads = page.xpath("//div[contains(@class, 'sect_body')]/div[contains(@class, 'imd') and not(contains(@class, 'typevip'))]")
        if not ads:
            raise StopIteration()

        for ad in ads:
            appartment = self.parse_ad(ad)
            if appartment:
                yield appartment

    def get_url(self, skip=0):
        parsed = urlparse.urlparse(self.url)
        return urlparse.urlunsplit((parsed.scheme, parsed.netloc, "{}{}".format(parsed.path, skip), parsed.query, parsed.fragment))

    def parse_ad(self, ad):
        apartment = Apartment()
        logging.debug(lxml.html.tostring(ad))
        apartment.link = ad.xpath("./div[contains(@class, 'imd_photo')]/a/@href")[0]
        apartment.photos = [ad.xpath("./div[contains(@class, 'imd_photo')]//img/@data-src")[0]]
        apartment.location = ad.xpath("./div[contains(@class, 'md_head')]")

        price = ad.xpath("./div[contains(@class, 'itm_price')]/text()")
        apartment.price = int(price[:-5].replace(' ', ''))
        apartment.currency = "BYN"
        return apartment

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    for ap in Neagent().find():
        print(ap)