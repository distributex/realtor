from .onliner import OnlinerPuller

html = """
    <!doctype html>
<!--[if IE 7 ]><html lang="ru" class="ie7 non-responsive-layout"><![endif]-->
<!--[if IE 8 ]><html lang="ru" class="ie8 non-responsive-layout"><![endif]-->
<!--[if IE 9 ]><html lang="ru" class="ie9 non-responsive-layout"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ru" class="non-responsive-layout"><!--<![endif]-->
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W4W6RXF');</script>
<!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1150,user-scalable=yes">
    <meta name="keywords" content="">
    <meta name="description" content="Комната, Минск, Сторожовская улица, 8. Снять комнату, аренда без посредников — Onliner.">
    <title>Снять комнату, Минск, Сторожовская улица, 8 без посредников</title>
    <link rel="shortcut icon" href="https://gc.onliner.by/favicon.ico">
    <!-- Icons -->
    <!-- Common and Chrome: Android-->
    <link rel="icon" type="image/x-icon" href="https://gc.onliner.by/images/logo/icons/favicon.ico">
    <link rel="icon" type="image/png" href="https://gc.onliner.by/images/logo/icons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="https://gc.onliner.by/images/logo/icons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="https://gc.onliner.by/images/logo/icons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="https://gc.onliner.by/images/logo/icons/favicon-192x192.png" sizes="192x192">

    <!-- Colors -->
    <!-- Chrome: Android-->
    <meta name="theme-color" content="#246eb7">

    <!-- Pinned Tabs -->
    <!-- Safari: OS X -->
    <link rel="mask-icon" href="https://gc.onliner.by/images/logo/icons/icon.svg" color="#ff0000">

    <link rel="stylesheet" href="//gc.onliner.by/assets/common_v3.e4973edefd112c17.css">
    <link rel="stylesheet" href="//gc.onliner.by/assets/non-responsive-layout.809ff4d6b2e995c0.css">
    <!--[if lt IE 9]><script src="https://gc.onliner.by/js/html5.js?token=1577096133"></script><![endif]-->
    <script src="//gc.onliner.by/assets/vendor.121859125e8061cd.js"></script>
    <script src="//gc.onliner.by/assets/common.197e65c82d058192.js"></script>
    
        <link rel="stylesheet" href="https://r.onliner.by/ak/assets/arenda.ef7ae8457145e25f.css">    <script async src="https://yastatic.net/pcode/adfox/header-bidding.js"></script>
<script>
var adfoxBiddersMap = {"myTarget":"871776","criteo":"816954"};
var adUnits= [{"code":"adfox_154116814371877731","bids":[{"bidder":"criteo","params":{"placementId":"1356663"}},{"bidder":"myTarget","params":{"placementId":"337002"}}]},{"code":"adfox_154047743399651128","bids":[{"bidder":"criteo","params":{"placementId":"1319354"}},{"bidder":"myTarget","params":{"placementId":"336926"}}]}];
var userTimeout = 1000;
window.YaHeaderBiddingSettings = {
    biddersMap: adfoxBiddersMap,
    adUnits: adUnits,
    timeout: userTimeout
};
</script>
<script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>
                    
    <!-- Safari: iOS -->
    <link rel="apple-touch-icon" sizes="60x60" href="https://gc.onliner.by/images/logo/icons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://gc.onliner.by/images/logo/icons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://gc.onliner.by/images/logo/icons/apple-touch-icon-180x180.png">
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W4W6RXF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!--Layout container-->
<div class="layout-container">
<!--Container-->
<div id="container">
    <div class="g-container-outer">

            <div class="bnr-top-wide bnr-top-wide_fixed-height"><div id="adfox_154047743399651128"></div>
<script>
    window.Ya.adfoxCode.createAdaptive({
        ownerId: 260941,
        containerId: 'adfox_154047743399651128',
        params: {
            p1: 'cbylc',
            p2: 'fzvf',
            puid1: 'ak'
        }
    }, ['desktop'], {
        tabletWidth: 1000,
        phoneWidth: 640,
        isAutoReloads: false
    });
</script>
</div>
    <div class="l-gradient-wrapper">
<!--Top-->
<header class="g-top">

    <!--Top-navigation-->
<div class="b-top-menu">
<div class="g-top-i">
<nav class="b-top-navigation">

<!--Main-navigation-->
<ul class="b-main-navigation">
<li class="b-main-navigation__item">
    <a href="https://catalog.onliner.by/cat/gifts?utm_source=bubble&utm_medium=happynewyear" class="b-main-navigation__advert b-main-navigation__advert_special">
        <div class="b-main-navigation__bubble b-main-navigation__bubble_special">
            Новый год!
        </div>
    </a>
    <a href="https://catalog.onliner.by/" class="b-main-navigation__link">
        <span class="b-main-navigation__text">Каталог</span>
    </a>
</li>

<li class="b-main-navigation__item b-main-navigation__item_arrow">
    <a href="https://www.onliner.by" class="b-main-navigation__link">
        <span class="b-main-navigation__text">Новости</span>
    </a>
    <div class="b-main-navigation__dropdown">
        <div class="g-top-i">
            <div class="b-main-navigation__dropdown-wrapper">
                <div class="b-main-navigation__dropdown-grid">
                                                                <div class="b-main-navigation__dropdown-column b-main-navigation__dropdown-column_25">
                            <div class="b-main-navigation__dropdown-title">
                                <a href="https://people.onliner.by" class="b-main-navigation__dropdown-title-link">Люди</a>
                            </div>
                            <ul class="b-main-navigation__dropdown-news-list">
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://people.onliner.by/2019/12/25/v-nepale" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/f24d55c258df2ce4ce29ae11794740d1.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_primary">25 724</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://people.onliner.by/2019/12/25/v-nepale" class="b-main-navigation__dropdown-news-link">Парня и девушку из Беларуси задержали с 6 килограммами кокаина в Непале</a>
            </div>
        </li>
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://people.onliner.by/2019/12/25/propal-rossiyanin" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/53707f89e71b5ed95d9954b196e9ddfe.png)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_primary">25 500</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://people.onliner.by/2019/12/25/propal-rossiyanin" class="b-main-navigation__dropdown-news-link">В Минске пропал россиянин, приехавший в Беларусь на заработки</a>
            </div>
        </li>
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://people.onliner.by/2019/12/25/uroven-integracii" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/da4d4e7f567b719d4c9418dff9253cef.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_primary">19 233</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://people.onliner.by/2019/12/25/uroven-integracii" class="b-main-navigation__dropdown-news-link">«Уровень интеграции настолько высок, что сложно избежать „шероховатостей“». Российская сторона ответила на критику Лукашенко</a>
            </div>
        </li>
    </ul>
<div class="b-main-navigation__dropdown-control">
    <a href="https://people.onliner.by" class="b-main-navigation__dropdown-button">
                    Еще 2 за сегодня
            </a>
</div>

                        </div>
                                            <div class="b-main-navigation__dropdown-column b-main-navigation__dropdown-column_25">
                            <div class="b-main-navigation__dropdown-title">
                                <a href="https://auto.onliner.by" class="b-main-navigation__dropdown-title-link">Авто</a>
                            </div>
                            <ul class="b-main-navigation__dropdown-news-list">
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://auto.onliner.by/2019/12/25/strashnoe-dtp" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/1f2ad13063917617bb82c5e2b37461b1.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_secondary">38 832</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://auto.onliner.by/2019/12/25/strashnoe-dtp" class="b-main-navigation__dropdown-news-link">Страшное ДТП в Витебской области: погиб 17-летний парень</a>
            </div>
        </li>
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://auto.onliner.by/2019/12/25/avtobaraxolka-34" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/cce3be9c70852731fcd7c1481a008e47.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_secondary">34 791</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://auto.onliner.by/2019/12/25/avtobaraxolka-34" class="b-main-navigation__dropdown-news-link">Выбираем «заряженную» немецкую машину </a>
            </div>
        </li>
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://auto.onliner.by/2019/12/25/lada-cybertruck" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/1dda216836f32a23185995f39419deca.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_primary">7024</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://auto.onliner.by/2019/12/25/lada-cybertruck" class="b-main-navigation__dropdown-news-link">Точно такой же, только лучше. В России плодятся двойники знаменитого Tesla Cybertruck</a>
            </div>
        </li>
    </ul>
<div class="b-main-navigation__dropdown-control">
    <a href="https://auto.onliner.by" class="b-main-navigation__dropdown-button">
                    Перейти в раздел
            </a>
</div>

                        </div>
                                            <div class="b-main-navigation__dropdown-column b-main-navigation__dropdown-column_25">
                            <div class="b-main-navigation__dropdown-title">
                                <a href="https://tech.onliner.by" class="b-main-navigation__dropdown-title-link">Технологии</a>
                            </div>
                            <ul class="b-main-navigation__dropdown-news-list">
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://tech.onliner.by/2019/12/25/best-2019" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/358623eafdb3236b283fe46cd417dc0f.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_primary">19 546</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://tech.onliner.by/2019/12/25/best-2019" class="b-main-navigation__dropdown-news-link">Все, от чего мы остались в восторге в 2019 году</a>
            </div>
        </li>
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://tech.onliner.by/2019/12/24/v-it-3" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/3628d552da417f1952f63f6ed1624aed.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_secondary">34 444</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://tech.onliner.by/2019/12/24/v-it-3" class="b-main-navigation__dropdown-news-link">Отработал сантехником 12 лет, а потом стал программистом</a>
            </div>
        </li>
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://tech.onliner.by/2019/12/24/apple-601" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/15b3b5eecfd10dba16799052c282d9f0.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_primary">17 153</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://tech.onliner.by/2019/12/24/apple-601" class="b-main-navigation__dropdown-news-link">Apple пять дней будет одаривать пользователей iOS</a>
            </div>
        </li>
    </ul>
<div class="b-main-navigation__dropdown-control">
    <a href="https://tech.onliner.by" class="b-main-navigation__dropdown-button">
                    Перейти в раздел
            </a>
</div>

                        </div>
                                            <div class="b-main-navigation__dropdown-column b-main-navigation__dropdown-column_25">
                            <div class="b-main-navigation__dropdown-title">
                                <a href="https://realt.onliner.by" class="b-main-navigation__dropdown-title-link">Недвижимость</a>
                            </div>
                            <ul class="b-main-navigation__dropdown-news-list">
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://realt.onliner.by/2019/12/25/prazdnik-3" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/201256567f12a6d3bdcdc836842a2283.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_primary">18 344</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://realt.onliner.by/2019/12/25/prazdnik-3" class="b-main-navigation__dropdown-news-link">Когда не все равно. Как минчане украсили свои подъезды к праздникам</a>
            </div>
        </li>
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://realt.onliner.by/2019/12/24/krik-dushi-28" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/fe0b100665c61e002a5da3425ee26ab8.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_secondary">60 288</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://realt.onliner.by/2019/12/24/krik-dushi-28" class="b-main-navigation__dropdown-news-link">Крик души молодого человека, ищущего «однушку»</a>
            </div>
        </li>
            <li class="b-main-navigation__dropdown-news-item">
            <a href="https://realt.onliner.by/2019/12/24/kvartira-54" class="b-main-navigation__dropdown-news-preview">
                <span class="b-main-navigation__dropdown-news-image" style="background-image:url(https://content.onliner.by/news/site_header/7a30ca55f76496babe87526533768307.jpeg)"></span>
                <span class="b-main-navigation__dropdown-news-labels">
                                            <span class="b-main-navigation__dropdown-news-label b-main-navigation__dropdown-news-label_view b-main-navigation__dropdown-news-label_secondary">56 177</span>
                                    </span>
            </a>
            <div class="b-main-navigation__dropdown-news-description">
                <a href="https://realt.onliner.by/2019/12/24/kvartira-54" class="b-main-navigation__dropdown-news-link">В Минске продают «квартиру с самым креативным дизайном»</a>
            </div>
        </li>
    </ul>
<div class="b-main-navigation__dropdown-control">
    <a href="https://realt.onliner.by" class="b-main-navigation__dropdown-button">
                    Перейти в раздел
            </a>
</div>

                        </div>
                                    </div>
            </div>
        </div>
    </div>
</li>

<li class="b-main-navigation__item b-main-navigation__item_arrow">
    <a href="https://ab.onliner.by/?utm_source=bubble&utm_medium=create" class="b-main-navigation__advert b-main-navigation__advert_secondary">
        <div class="b-main-navigation__bubble b-main-navigation__bubble_secondary">
            Продать авто
        </div>
    </a>
<a href="https://ab.onliner.by" class="b-main-navigation__link">
    <span class="b-main-navigation__text">Автобарахолка</span>
</a>
<div class="b-main-navigation__dropdown">
<div class="g-top-i">
<div class="b-main-navigation__dropdown-wrapper">
<div class="b-main-navigation__dropdown-grid">
<div class="b-main-navigation__dropdown-column b-main-navigation__dropdown-column_75">
    <div class="b-main-navigation__dropdown-title">
        <a href="https://ab.onliner.by" class="b-main-navigation__dropdown-title-link">Автобарахолка</a>
    </div>
    <div class="b-main-navigation__dropdown-wrapper">
    <div class="b-main-navigation__dropdown-grid">
            <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/audi" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Audi</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;2265</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/bmw" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">BMW</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;2612</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/chrysler" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Chrysler</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;350</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/citroen" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Citroen</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;1436</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/dodge" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Dodge</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;276</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/fiat" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Fiat</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;723</span>
                        </a>
                    </li>
                            </ul>
        </div>
            <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/ford" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Ford</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;2469</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/honda" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Honda</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;713</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/hyundai" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Hyundai</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;1000</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/kia" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Kia</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;847</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/mazda" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Mazda</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;1224</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/mercedes-benz" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Mercedes</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;2198</span>
                        </a>
                    </li>
                            </ul>
        </div>
            <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/mitsubishi" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Mitsubishi</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;813</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/nissan" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Nissan</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;1301</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/opel" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Opel</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;2413</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/peugeot" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Peugeot</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;1716</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/renault" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Renault</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;2286</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/rover" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Rover</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;198</span>
                        </a>
                    </li>
                            </ul>
        </div>
            <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/seat" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Seat</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;258</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/skoda" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Skoda</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;1183</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/toyota" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Toyota</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;1284</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/volvo" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Volvo</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;858</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/volkswagen" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Volkswagen</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;3780</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://ab.onliner.by/lada" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">LADA</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;155</span>
                        </a>
                    </li>
                            </ul>
        </div>
        </div>
</div>
    <div class="b-main-navigation__dropdown-control">
        <a href="https://ab.onliner.by" class="b-main-navigation__dropdown-button">36 634 объявления</a>
    </div>
</div>
<div class="b-main-navigation__dropdown-column b-main-navigation__dropdown-column_25">
    <div class="b-main-navigation__dropdown-title">
        <a href="https://mb.onliner.by" class="b-main-navigation__dropdown-title-link">Мотобарахолка</a>
    </div>
    <div class="b-main-navigation__dropdown-wrapper">
    <div class="b-main-navigation__dropdown-grid">
            <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][2]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Aprilia</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;27</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][7]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">BMW</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;130</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][17]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Harley-Davidson</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;108</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][18]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Honda</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;599</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][19]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">HORS</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;53</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][67]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Jawa</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;26</span>
                        </a>
                    </li>
                            </ul>
        </div>
            <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][24]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Kawasaki</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;304</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][49]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Suzuki</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;359</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][57]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Viper</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;13</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][59]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Yamaha</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;344</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][70]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Днепр</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;41</span>
                        </a>
                    </li>
                                    <li class="b-main-navigation__dropdown-advert-item">
                        <a href="https://mb.onliner.by#moto[0][63]=" class="b-main-navigation__dropdown-advert-link b-main-navigation__dropdown-advert-link_filter">
                            <span class="b-main-navigation__dropdown-advert-sign">Минск</span>
                            <span class="b-main-navigation__dropdown-advert-value">&nbsp;112</span>
                        </a>
                    </li>
                            </ul>
        </div>
        </div>
</div>
    <div class="b-main-navigation__dropdown-control">
        <a href="https://mb.onliner.by" class="b-main-navigation__dropdown-button">2761 объявление</a>
    </div>
</div>
</div>
</div>
</div>
</div>
</li>
<li class="b-main-navigation__item b-main-navigation__item_arrow b-main-navigation__item_current">
<a href="https://r.onliner.by/pk" class="b-main-navigation__link">
    <span class="b-main-navigation__text">Дома и квартиры</span>
</a>
<div class="b-main-navigation__dropdown">
<div class="g-top-i">
<div class="b-main-navigation__dropdown-wrapper">
<div class="b-main-navigation__dropdown-grid">

<div class="b-main-navigation__dropdown-column b-main-navigation__dropdown-column_50">
    <div class="b-main-navigation__dropdown-title">
        <a href="https://r.onliner.by/pk" class="b-main-navigation__dropdown-title-link">Продажа</a>
    </div>
    <div class="b-main-navigation__dropdown-wrapper">
    <div class="b-main-navigation__dropdown-grid">
        <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                                                            <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#bounds%5Blb%5D%5Blat%5D=53.820922446131&bounds%5Blb%5D%5Blong%5D=27.344970703125&bounds%5Brt%5D%5Blat%5D=53.97547425743&bounds%5Brt%5D%5Blong%5D=27.77961730957" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Минск</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;6908</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#bounds%5Blb%5D%5Blat%5D=51.941725203142&bounds%5Blb%5D%5Blong%5D=23.492889404297&bounds%5Brt%5D%5Blat%5D=52.234528294214&bounds%5Brt%5D%5Blong%5D=23.927536010742" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Брест</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;3380</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#bounds%5Blb%5D%5Blat%5D=55.085834940707&bounds%5Blb%5D%5Blong%5D=29.979629516602&bounds%5Brt%5D%5Blat%5D=55.357648391381&bounds%5Brt%5D%5Blong%5D=30.414276123047" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Витебск</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;291</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#bounds%5Blb%5D%5Blat%5D=52.302600726968&bounds%5Blb%5D%5Blong%5D=30.732192993164&bounds%5Brt%5D%5Blat%5D=52.593037841157&bounds%5Brt%5D%5Blong%5D=31.166839599609" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Гомель</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;569</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#bounds%5Blb%5D%5Blat%5D=53.538267122397&bounds%5Blb%5D%5Blong%5D=23.629531860352&bounds%5Brt%5D%5Blat%5D=53.820517109806&bounds%5Brt%5D%5Blong%5D=24.064178466797" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Гродно</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;120</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#bounds%5Blb%5D%5Blat%5D=53.74261986683&bounds%5Blb%5D%5Blong%5D=30.132064819336&bounds%5Brt%5D%5Blat%5D=54.023503252809&bounds%5Brt%5D%5Blong%5D=30.566711425781" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Могилев</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;214</span>
                            </a>
                        </li>
                                                </ul>
        </div>
                <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                                            <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/?number_of_rooms%5B%5D=1" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">1-комнатные</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;3453</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/?number_of_rooms%5B%5D=2" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">2-комнатные</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;5249</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/?number_of_rooms%5B%5D=3" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">3-комнатные</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;5201</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/?number_of_rooms%5B%5D=4&number_of_rooms%5B%5D=5&number_of_rooms%5B%5D=6" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">4+-комнатные</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;3344</span>
                            </a>
                        </li>
                                                </ul>
        </div>
        <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                                            <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#price%5Bmax%5D=30000&currency=usd" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">До 30 000 $</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;3823</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#price%5Bmin%5D=30000&price%5Bmax%5D=80000&currency=usd" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">30 000–80 000 $</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;8851</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/pk/#price%5Bmin%5D=80000&currency=usd" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">От 80 000 $</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;4808</span>
                            </a>
                        </li>
                                                </ul>
        </div>
    </div>
</div>
<div class="b-main-navigation__dropdown-control">
    <a href="https://r.onliner.by/pk" class="b-main-navigation__dropdown-button">
        17247 объявлений    </a>
</div>
</div>
<div class="b-main-navigation__dropdown-column b-main-navigation__dropdown-column_50">
    <div class="b-main-navigation__dropdown-title">
        <a href="https://r.onliner.by/ak" class="b-main-navigation__dropdown-title-link">Аренда</a>
    </div>
    <div class="b-main-navigation__dropdown-wrapper">
    <div class="b-main-navigation__dropdown-grid">
        <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                                                            <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#bounds%5Blb%5D%5Blat%5D=53.820922446131&bounds%5Blb%5D%5Blong%5D=27.344970703125&bounds%5Brt%5D%5Blat%5D=53.97547425743&bounds%5Brt%5D%5Blong%5D=27.77961730957" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Минск</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;1731</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#bounds%5Blb%5D%5Blat%5D=51.941725203142&bounds%5Blb%5D%5Blong%5D=23.492889404297&bounds%5Brt%5D%5Blat%5D=52.234528294214&bounds%5Brt%5D%5Blong%5D=23.927536010742" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Брест</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;23</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#bounds%5Blb%5D%5Blat%5D=55.085834940707&bounds%5Blb%5D%5Blong%5D=29.979629516602&bounds%5Brt%5D%5Blat%5D=55.357648391381&bounds%5Brt%5D%5Blong%5D=30.414276123047" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Витебск</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;12</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#bounds%5Blb%5D%5Blat%5D=52.302600726968&bounds%5Blb%5D%5Blong%5D=30.732192993164&bounds%5Brt%5D%5Blat%5D=52.593037841157&bounds%5Brt%5D%5Blong%5D=31.166839599609" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Гомель</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;20</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#bounds%5Blb%5D%5Blat%5D=53.538267122397&bounds%5Blb%5D%5Blong%5D=23.629531860352&bounds%5Brt%5D%5Blat%5D=53.820517109806&bounds%5Brt%5D%5Blong%5D=24.064178466797" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Гродно</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;11</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#bounds%5Blb%5D%5Blat%5D=53.74261986683&bounds%5Blb%5D%5Blong%5D=30.132064819336&bounds%5Brt%5D%5Blat%5D=54.023503252809&bounds%5Brt%5D%5Blong%5D=30.566711425781" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Могилев</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;14</span>
                            </a>
                        </li>
                                                </ul>
        </div>
                <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                                            <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/?rent_type%5B%5D=1_room" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">1-комнатные</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;426</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/?rent_type%5B%5D=2_rooms" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">2-комнатные</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;655</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/?rent_type%5B%5D=3_rooms" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">3-комнатные</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;481</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/?rent_type%5B%5D=4_rooms&rent_type%5B%5D=5_rooms&rent_type%5B%5D=6_rooms" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">4+-комнатные</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;274</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/?rent_type%5B%5D=room" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">Комнаты</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;156</span>
                            </a>
                        </li>
                                                </ul>
        </div>

        <div class="b-main-navigation__dropdown-column">
            <ul class="b-main-navigation__dropdown-advert-list">
                                <li class="b-main-navigation__dropdown-advert-item">
                    <a href="https://r.onliner.by/ak/?only_owner=true" class="b-main-navigation__dropdown-advert-link">
                        <span class="b-main-navigation__dropdown-advert-sign">От собственника</span>
                        <span class="b-main-navigation__dropdown-advert-value">&nbsp;1242</span>
                    </a>
                </li>
                
                                                            <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#price%5Bmax%5D=250&currency=usd" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">До 250 $</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;317</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#price%5Bmin%5D=250&price%5Bmax%5D=500&currency=usd" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">250-500 $</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;732</span>
                            </a>
                        </li>
                                                                                <li class="b-main-navigation__dropdown-advert-item">
                            <a href="https://r.onliner.by/ak/#price%5Bmin%5D=500&currency=usd" class="b-main-navigation__dropdown-advert-link">
                                <span class="b-main-navigation__dropdown-advert-sign">От 500 $</span>
                                <span class="b-main-navigation__dropdown-advert-value">&nbsp;1099</span>
                            </a>
                        </li>
                                                </ul>
        </div>
    </div>
</div>

<div class="b-main-navigation__dropdown-control">
    <a href="https://r.onliner.by/ak" class="b-main-navigation__dropdown-button">
        1992 объявления    </a>
</div>
</div>

</div>
</div>
</div>
</div>
</li>
<li class="b-main-navigation__item">
    <a href="https://s.onliner.by/tasks" class="b-main-navigation__link">
        <span class="b-main-navigation__text">Услуги</span>
    </a>
</li>
<li class="b-main-navigation__item">
    <a href="https://baraholka.onliner.by/" class="b-main-navigation__link">
        <span class="b-main-navigation__text">Барахолка</span>
    </a>
</li>
<li class="b-main-navigation__item">
    <a href="https://forum.onliner.by/" class="b-main-navigation__link">
        <span class="b-main-navigation__text">Форум</span>
    </a>
</li>
</ul>
<!--/Main-navigation-->

<!-- Huawei -->
<a href="https://huawei.onliner.by/" class="b-top-navigation-huawei"></a>
<!-- / Huawei -->

<!-- Informers -->
<ul class="b-top-navigation-informers helpers_hide_desktop">
    <li class="b-top-navigation-informers__item top-informer-currency" id="currency-informer"
    data-bind="css: {'up': $root.grow === 1,
                     'down': $root.grow === -1}">
    <a class="b-top-navigation-informers__link" href="https://kurs.onliner.by/" data-bind="visible: true">
        <span class="_u" data-bind="text: '$ ' + $root.amount"></span>
    </a>
</li>
<li class="b-top-navigation-informers__item top-informer-weather" id="weather-informer">
    <a class="b-top-navigation-informers__link" href="https://pogoda.onliner.by/" data-bind="visible: true" style="display: none;">
        <i data-bind="attr: {'title': 'Погода в ' + $root.city, class: 'phenomena-icon extra-small ' + $root.icon}"></i>
        <span class="_u" data-bind="text: $root.temperature + '&deg;'"></span>
    </a>
</li>
</ul>
<!-- / Informers -->

<!-- Age -->
<div class="b-top-navigation-age">18+</div>
<!-- / Age -->

<!--Cart-navigation-->
<ul class="b-top-navigation-cart" id="b-top-navigation-cart">
    <li class="b-top-navigation-cart__item" style="display: none;"
        data-bind="css: {'b-top-navigation-cart__item_active': $root.quantity()},
                         visible: $root.quantity() || $root.quantity() === 0">
        <a href="https://cart.onliner.by/" class="b-top-navigation-cart__link">
            <!-- ko if: $root.quantity() -->
            <span data-bind="text: $root.quantity() + ' ' + format.pluralForm($root.quantity(), ['товар', 'товара', 'товаров'])"></span>
            <!-- /ko -->

            <!-- ko if: !$root.quantity() -->
            <span>Корзина</span>
            <!-- /ko -->
        </a>
    </li>
</ul>
<!--/Cart-navigation-->
</nav>
</div>
</div>
<!--/Top-navigation-->

    <div class="b-top-actions">
        <div class="g-top-i">
            <div class="b-top-logo">
    <a href="https://www.onliner.by/">
        <img srcset="https://gc.onliner.by/images/logo/onliner_logo.v3.png?token=1577096133,
                     https://gc.onliner.by/images/logo/onliner_logo.v3@2x.png?token=1577096133 2x"
             src="https://gc.onliner.by/images/logo/onliner_logo.v3@2x.png?token=1577096133" width="180" height="40" alt="Onliner" class="onliner_logo">
    </a>
</div>
            <div class="b-top-wrapper">
                
<div id="fast-search" class="fast-search">
    <!-- .fast-search_legacy for legacy design -->
    <form class="fast-search__form" action="//catalog.onliner.by/search/" method="get" autocomplete="off">
        <input class="fast-search__input" type="text" placeholder="Поиск в Каталоге. Например, &quot;телевизор 42&quot;" tabindex="1" name="query"
               value=""
               data-project="apartment_rentals">
        <input type="hidden" name="charset" value="utf-8">
        <button class="fast-search__submit" type="submit">Найти</button>
    </form>
</div>
<div id="userbar" class="b-top-profile js-userbar">
    <div class="b-top-profile__list">
        <!-- ko if: $root.currentUser.id() -->
        <div class="b-top-profile__item b-top-profile__item_arrow"
             data-bind="visible: $root.currentUser.id()"
             style="display: none;">

            <a href="https://profile.onliner.by" class="b-top-profile__preview js-toggle-bar">
                <div class="b-top-profile__image js-header-user-avatar"
                     data-bind="style: { backgroundImage: 'url(\'https://content.onliner.by/user/avatar/60x60/' + $root.currentUser.id() + '?v=' +$root.randomHash() + '\')' }">
                </div>
            </a>

            <div class="b-top-profile__dropdown b-top-profile__dropdown_user js-stop-propagation js-top-profile-dropdown">
                <div class="b-top-profile__header">
                    <div class="b-top-profile__flex">
                        <div class="b-top-profile__part b-top-profile__part_1">
                            <div class="b-top-profile__name">
                                <a href="https://profile.onliner.by"
                                   class="b-top-profile__link b-top-profile__link_alter"
                                   data-bind="html: $root.currentUser.nickname">
                                </a>
                            </div>
                        </div>

                        <div class="b-top-profile__part b-top-profile__part_2">
                            <div class="b-top-profile__logout">
                                <a class="b-top-profile__link b-top-profile__link_secondary"
                                   data-bind="click: $root.currentUser.logout">
                                    Выйти
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="b-top-profile__close"></div>
                </div>
                <div class="b-top-profile__body">
                    <div class="b-top-profile__overflow js-top-profile-overflow">

                        
                        <div class="b-top-profile__sublist">

                            <!-- ko if: $root.cartQuantity() !== undefined && $root.cartQuantity() > 0 -->
                            <div class="b-top-profile__subitem b-top-profile__subitem_basket">
                                <a href="https://cart.onliner.by" class="b-top-profile__link b-top-profile__link_alter">
                                    <span data-bind="text: $root.cartQuantity() + ' ' + format.pluralForm($root.cartQuantity(), ['товар в корзине', 'товара в корзине', 'товаров в корзине'])"></span>
                                </a>
                            </div>
                            <!-- /ko -->

                            <!-- ko if: !$root.cartQuantity() -->
                            <div class="b-top-profile__subitem b-top-profile__subitem_basket">
                                <a href="https://cart.onliner.by" class="b-top-profile__link b-top-profile__link_alter">
                                    Корзина
                                </a>
                            </div>
                            <!-- /ko -->

                            
                            <div class="b-top-profile__subitem b-top-profile__subitem_history">
                                <a href="https://cart.onliner.by/orders" class="b-top-profile__link b-top-profile__link_alter">
                                    История заказов
                                </a>
                            </div>
                            <div class="b-top-profile__subitem b-top-profile__subitem_subscriptions">
                                <a href="https://profile.onliner.by/subscriptions" class="b-top-profile__link b-top-profile__link_alter">
                                    Список подписок
                                </a>
                            </div>
                        </div>
                        <div class="b-top-profile__divider"></div>
                        <div class="b-top-profile__sublist">
                            <div class="b-top-profile__subitem b-top-profile__subitem_arrow">
                                <span class="b-top-profile__link b-top-profile__link_alter js-open-submenu">
                                    Мои объявления
                                </span>
                                <div class="b-top-profile__sublist">
                                    <div class="b-top-profile__subitem">
                                        <a class="b-top-profile__link b-top-profile__link_alter"
                                           data-bind="attr: {href: 'https://baraholka.onliner.by/search.php?type=ufleamarket&id=' + $root.currentUser.id()}">
                                            Объявления в барахолке
                                        </a>
                                    </div>
                                    <div class="b-top-profile__subitem">
                                        <a class="b-top-profile__link b-top-profile__link_alter"
                                           data-bind="attr: {href: 'https://catalog.onliner.by/used/users/' + $root.currentUser.id()}">
                                            Объявления в каталоге
                                        </a>
                                    </div>
                                    <div class="b-top-profile__subitem">
                                        <a href="https://ab.onliner.by/mycars"
                                           class="b-top-profile__link b-top-profile__link_alter">
                                            Автообъявления
                                        </a>
                                    </div>
                                    <div class="b-top-profile__subitem">
                                        <a class="b-top-profile__link b-top-profile__link_alter"
                                           data-bind="attr: {href: 'https://mb.onliner.by/mymoto/' + $root.currentUser.id()}">
                                            Мотообъявления
                                        </a>
                                    </div>
                                    <div class="b-top-profile__subitem">
                                        <a class="b-top-profile__link b-top-profile__link_alter"
                                           data-bind="attr: {href: 'https://r.onliner.by/ak/users/' + $root.currentUser.id() + '/apartments'}">
                                            Oбъявления об Аренде квартир
                                        </a>
                                    </div>
                                    <div class="b-top-profile__subitem">
                                        <a class="b-top-profile__link b-top-profile__link_alter"
                                           data-bind="attr: {href: 'https://r.onliner.by/pk/users/' + $root.currentUser.id() + '/apartments'}">
                                            Oбъявления о Продаже квартир
                                        </a>
                                    </div>
                                    <div class="b-top-profile__subitem">
                                        <a href="https://s.onliner.by/tasks?my_tasks=true"
                                           class="b-top-profile__link b-top-profile__link_alter">
                                            Заказы на сервисе «Услуги»
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-top-profile__divider"></div>
                        <div class="b-top-profile__sublist">
                            <div class="b-top-profile__subitem">
                                <a href="https://profile.onliner.by/reviews/shop"
                                   class="b-top-profile__link b-top-profile__link_alter">
                                    Отзывы на магазины
                                </a>
                            </div>
                            <div class="b-top-profile__subitem">
                                <a class="b-top-profile__link b-top-profile__link_alter"
                                   data-bind="attr: {href: 'https://forum.onliner.by/search.php?type=utopics&id=' + $root.currentUser.id()}">
                                    Темы на форуме
                                </a>
                            </div>
                            <div class="b-top-profile__subitem">
                                <a class="b-top-profile__link b-top-profile__link_alter"
                                   data-bind="attr: {href: 'https://forum.onliner.by/search.php?type=uposts&id=' + $root.currentUser.id()}">
                                    Сообщения на форуме и барахолке
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="b-top-profile__footer js-top-profile-footer">
                    <div class="b-top-profile__flex">
                        <div class="b-top-profile__part b-top-profile__part_1">
                            <a title="Редактировать личные данные" href="https://profile.onliner.by/personal" class="b-top-profile__settings"></a>
                        </div>
                        <div class="b-top-profile__part b-top-profile__part_2">
                            <div class="b-top-profile__rules">
                                <a title="Пользовательское соглашение" href="https://blog.onliner.by/siterules" class="b-top-profile__link b-top-profile__link_secondary">
                                    Пользовательское соглашение
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style>
            [v-cloak] {
                display: none;
            }
        </style>

        <div id="global-chat-app"
             class="b-top-profile__item"
             data-bind="visible: $root.currentUser.id()"
             style="display: none;">
            <a href="https://chats.onliner.by" class="b-top-profile__message"
               v-if="false"></a>

            <chat-counter inline-template
                          v-cloak>
                <a href="https://chats.onliner.by" class="b-top-profile__message"
                   v-if="isMobile || isMobileLayout">
                    <span class="b-top-profile__counter" v-cloak
                          v-show="newMessagesCounter">{{ newMessagesCounter }}</span>
                </a>

                <a href="https://chats.onliner.by" class="b-top-profile__message js-toggle-bar"
                   v-else
                   @click="initApp">
                    <span class="b-top-profile__counter" v-cloak
                          v-show="newMessagesCounter">{{ newMessagesCounter }}</span>
                </a>
            </chat-counter>

            <template v-if="!isMobile && !isMobileLayout">
                <div class="b-top-profile__dropdown b-top-profile__dropdown_message js-top-profile-dropdown">
    <div class="b-top-profile__header">
        <div class="b-top-profile__flex">
            <div class="b-top-profile__part b-top-profile__part_1">
                <div class="b-top-profile__title"
                     v-cloak
                     v-if="isMobileLayout">
                    Чаты
                </div>
                <div class="b-top-profile__title"
                     v-else>
                    <a href="https://chats.onliner.by"
                       class="b-top-profile__link b-top-profile__link_alter">
                        Чаты
                    </a>
                </div>
            </div>
            <div class="b-top-profile__part b-top-profile__part_2"></div>
        </div>
        <div class="b-top-profile__close js-close-popup"></div>
    </div>
    <div class="b-top-profile__body">
        <div class="b-top-profile__overflow">
            <div class="chat-offers"
                 v-cloak
                 :class="{ 'chat-offers_animated': listIsLoading}">
                <template v-if="selectedGroup.chats">
                    <chat-list-group-header :group="selectedGroup"></chat-list-group-header>

                    <div class="chat-offers__body">
                        <chat-list :chats-list="selectedGroup"
                                   :is-group="true"
                                   :do-show-list="true"
                                   :group-key="selectedGroup.groupKey"></chat-list>
                    </div>
                </template>

                <div class="chat-offers__body" v-else>
                    <chat-list :chats-list="rootChatList"
                               :do-show-list="true"
                               :additional-classes="'js-top-profile-overflow'"></chat-list>
                </div>
            </div>
        </div>
    </div>
</div>
            </template>
        </div>

        <!-- ko with: $root.notificationsList -->
<div id="notifications-list"
     class="b-top-profile__item"
     data-bind="visible: $root.currentUser.id()"
     style="display: none;">
    <a href="#" class="b-top-profile__notice js-toggle-bar">
        <span class="b-top-profile__counter"
              data-bind="text: $data.newFeedsCounter() > 99 ? '99+' : $data.newFeedsCounter(),
                         visible: $data.newFeedsCounter()"></span>
    </a>
    <div class="b-top-profile__dropdown b-top-profile__dropdown_notice js-top-profile-dropdown">
        <div class="b-top-profile__header">
            <div class="b-top-profile__flex">
                <div class="b-top-profile__part b-top-profile__part_1">
                    <div class="b-top-profile__title">
                        Уведомления
                    </div>
                </div>
                <div class="b-top-profile__part b-top-profile__part_2">
                    <div class="b-top-profile__select">
                        <a href="#" class="b-top-profile__link b-top-profile__link_secondary"
                           data-bind="click: $data.markAllAsRead.bind($data)">
                            Отметить все как прочитанные
                        </a>
                    </div>
                </div>
            </div>
            <div class="b-top-profile__close js-close-popup"></div>
        </div>
        <div class="b-top-profile__body">
            <div class="b-top-profile__overflow js-notifications-scrollable js-top-profile-overflow">
                <div class="b-top-profile__control">
                    <a href="#" class="b-top-profile__button b-top-profile__button_new" style="display: none"
                       data-bind="visible: $data.newMessagesButtonVisible() && $data.newFeedsCounter(),
                              text: $data.newMessagesButtonText(),
                              click: $data.updateNotificationsList.bind($data)"></a>
                </div>
                <div class="notice-offers"
                     data-bind="css: { 'notice-offers_animated': $data.isAnimated() }">
                    <div class="notice-offers__list js-notifications-scrollable-content"
                         data-bind="foreach: { data: $data.notificationsList, as: 'notification' }">
                        <a class="notice-offers__item"
                           data-bind="css: {
                                        'notice-offers__item_success': notification.type === 'positive',
                                        'notice-offers__item_warning': notification.type === 'warning',
                                        'notice-offers__item_error': notification.type === 'negative',
                                        'notice-offers__item_info': notification.type === 'general',
                                        'notice-offers__item_visited': notification.isRead() || (!notification.url && !notification.isNew())
                                      },
                                      attr: { href: notification.url },
                                      style: { 'cursor': notification.url ? 'pointer' : 'default' },
                                      click: $parent.onFeedClick.bind($parent)">
                            <div class="notice-offers__details">
                                <div class="notice-offers__details-list">
                                    <div class="notice-offers__details-item"
                                         data-bind="text: notification.project"></div>
                                    <div class="notice-offers__details-item"
                                         data-bind="text: notification.updatedAt"></div>
                                </div>
                            </div>
                            <div class="notice-offers__description"
                                 data-bind="html: notification.message"></div>
                        </a>
                    </div>
                    <div class="notice-offers__loader"
                         data-bind="visible: !$data.isAnimated() && $data.isProcessing()"></div>
                    <div class="notice-offers__message"
                         data-bind="visible: !$data.isAnimated() && !$data.isProcessing() && !$data.notificationsList().length">
                        <div class="notice-offers_message-content">
                            Нет уведомлений
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /ko -->

        <div class="b-top-profile__item"
             data-bind="visible: $root.currentUser.id()"
             style="display: none;">
            <a href="https://profile.onliner.by/bookmarks" class="b-top-profile__favorites"></a>
        </div>
        <!-- /ko-->

        <div id="cart-desktop"
             class="b-top-profile__item"
             data-bind="visible: true"
             style="display: none;">
            <a href="https://cart.onliner.by" class="b-top-profile__cart">
                <!-- ko if: $root.cartQuantity -->
                <span class="b-top-profile__counter"
                      data-bind="text: $root.cartQuantity"></span>
                <!-- /ko -->
            </a>
        </div>
    </div>

    <div class="auth-bar auth-bar--top" data-bind="visible: !$root.currentUser.id()" style="display: none;">
        <div id="login-app"></div>
    </div>

    <div class="auth-bar auth-bar--top"  data-bind="visible: !$root.currentUser.id()" style="display: none;">
        <div class="auth-bar auth-bar--top">
            <a href="https://cart.onliner.by" title="Корзина" class="auth-bar__item auth-bar__item--cart">
                <!-- ko if: $root.cartQuantity -->
                    <div class="auth-bar__counter"
                         data-bind="text: $root.cartQuantity">
                        <span class="b-top-profile__counter"></span>
                    </div>
                <!-- /ko -->
            </a>
        </div>
    </div>
</div>



<script>
    
    
    document.addEventListener('DOMContentLoaded', function () {
        var eventDispatcher = window.notificationsService.getEventDispatcher();

        window.userbar = window.userbar || {};
        window.userbar.currentUser = MODELS.currentUser;
        window.userbar.socialNetworksAuth = MODELS.socialNetworksAuth;
        window.userbar.cartQuantity = window.userbar.cartQuantity || ko.observable();
        window.userbar.tenderQuantity = window.userbar.tenderQuantity || ko.observable();
        window.userbar.notificationsList = new NotificationsList(eventDispatcher);
        window.userbar.randomHash = ko.observable(Math.random());

        window.userbar.setNickname = function (nickname) {
            MODELS.currentUser.nickname(nickname);
        };

        window.userbar.updateUserAvatar = function () {
            this.randomHash(Math.random());
        };

        var networks = window.userbar.socialNetworksAuth.networks;

        networks.facebook.sessionId = 'Ba5l7b6v1dK8wnYmLyZA';
        networks.vkontakte.sessionId = '9OvP0prf6ujQKbgYl1m4';
        networks.twitter.sessionId = '1bQpyDH4iAdSUF5jPzqt';

        networks.facebook.url = "https://user.api.onliner.by/socials/facebook/login?session_id=Ba5l7b6v1dK8wnYmLyZA";
        networks.vkontakte.url = "https://user.api.onliner.by/socials/vkontakte/login?session_id=9OvP0prf6ujQKbgYl1m4";
        networks.twitter.url = "https://user.api.onliner.by/socials/twitter/login?session_id=1bQpyDH4iAdSUF5jPzqt";

        ko.applyBindings(window.userbar, document.getElementById('userbar'));

            });
</script>

<div id="guard-app"></div>

            </div>
        </div>
    </div>
</header>
<!--/Top-->

<!--Middle-->
<div class="g-middle">
    <div class="g-middle-i">


    <script src="https://r.onliner.by/ak/assets/show.0f381b0dce61fedf.js"></script>

    <div class="project-navigation">
    <div class="project-navigation__flex">
        <div class="project-navigation__part project-navigation__part_1">
            <ul class="project-navigation__list project-navigation__list_primary">
                <li class="project-navigation__item project-navigation__item_primary">
                    <a href="https://r.onliner.by/pk/" class="project-navigation__link project-navigation__link_primary">
                        <span class="project-navigation__text">
                            <span class="project-navigation__sign">Продажа</span>
                        </span>
                    </a>
                </li>
                <li class="project-navigation__item project-navigation__item_primary project-navigation__item_active">
                    <a href="https://r.onliner.by/ak/" class="project-navigation__link project-navigation__link_primary">
                        <span class="project-navigation__text">
                            <span class="project-navigation__sign">Аренда</span>
                        </span>
                    </a>
                </li>
                            </ul>
        </div>
        <div class="project-navigation__part project-navigation__part_2">
                        <a href="https://r.onliner.by/ak/apartments/create" class="project-navigation__button"
               onclick="window.ga && window.ga('ak.send', 'event', 'button', 'ad');">
                Разместить объявление</a>
                    </div>
    </div>
</div>

    <div data-app class="arenda-apartment">
        <!--Classified: edit-->
<div class="apartment-edit">
    <div class="apartment-edit__menu">

        
        <div class="apartment-edit__item">
            <div class="apartment-edit__time apartment-edit__time_up" id="apartment-up__last-time">2 дня назад</div>
        </div>

                    <div class="apartment-edit__item" id="apartment-counter">
                <span class="apartment-edit__views-counter"></span>
            </div>
        
        
        
        
            </div>

    
    
    <script>
        $(function () {
            var isModerator = false,
                data = {
                    id: 500310,
                    can_be_reopened: false,
                    deleted_at: ''
                },
                callbacks = {},
                apartmentClose,
                elementsId = ['apartment-close-button', 'apartment-reopen-button', 'apartment-remove-popover'];

            callbacks.delete = {
                success: function (data) {
                    $('#apartment-updated-at').text('0 секунд назад');
                    $('#apartment-counter').hide();
                    $('#apartment-up').hide();
                }
            };

            callbacks.reopen = {
                success: function (data) {
                    $('#apartment-updated-at').text('0 секунд назад');
                    $('#apartment-up__last-time').text(moment(data.last_time_up).fromNow());
                }
            };

            apartmentClose = new ApartmentClose(data, isModerator, callbacks);

            ko.utils.arrayForEach(elementsId, function (id) {
                var element = document.getElementById(id);

                element && ko.applyBindings(apartmentClose, element);
            });
        });

        $(function () {
            var data = {
                    id: 500310,
                    time: ""
                },
                callbacks,
                apartmentUp,
                elementsId = ['apartment-up', 'apartment-up-popover'];

            callbacks = {
                success: function (data) {
                    $('#apartment-up__last-time').text(moment(data.last_time_up).fromNow());
                }
            };

            apartmentUp = new ApartmentUp(data, callbacks);

            ko.utils.arrayForEach(elementsId, function (id) {
                var element = document.getElementById(id);

                element && ko.applyBindings(apartmentUp, element);
            });
        });
    </script>
</div>
<!--Classified: edit END-->
        <!--Classified: cover-->
            <div style="background-image:url(https://content.onliner.by/apartment_rentals/2787709/1400x930/eb7c439826165eea413cec00c8005db5.jpeg)" class="apartment-cover">
            <span data-loupe="data-loupe" class="apartment-cover__loupe" aria-hidden="true"></span>
            <div class="apartment-cover__thumbnails">
                <div class="apartment-cover__thumbnails-inner">
                                                                                                        <div style="background-image: url(https://content.onliner.by/apartment_rentals/2787709/600x400/eb7c439826165eea413cec00c8005db5.jpeg)" class="apartment-cover__thumbnail"></div>
                                                                                                                                        <div style="background-image: url(https://content.onliner.by/apartment_rentals/2787709/600x400/cc3c97403e86efbec0e8bc602ba83f69.jpeg)" class="apartment-cover__thumbnail"></div>
                                                                    </div>
                <span class="apartment-cover__thumbnails-shadow"></span>
            </div>
        </div>
          <!--Classified: cover END-->
        <!-- gallery-->
<div class="apartment-gallery">
    <div class="apartment-gallery__close">&times;</div>
    <div data-auto="false" data-width="100%" data-height="100%" data-minheight="500px" data-maxheight="100%"
         data-thumbwidth="90" data-thumbheight="60" data-nav="thumbs"
         data-keyboard="true" data-arrows="true" class="fotorama">
                                                                            <div style="background-image: url(https://content.onliner.by/apartment_rentals/2787709/1400x930/eb7c439826165eea413cec00c8005db5.jpeg)"
                         data-thumb="https://content.onliner.by/apartment_rentals/2787709/600x400/eb7c439826165eea413cec00c8005db5.jpeg" class="apartment-gallery__slide">
                        &nbsp;</div>
                                                                                                    <div style="background-image: url(https://content.onliner.by/apartment_rentals/2787709/1400x930/cc3c97403e86efbec0e8bc602ba83f69.jpeg)"
                         data-thumb="https://content.onliner.by/apartment_rentals/2787709/600x400/cc3c97403e86efbec0e8bc602ba83f69.jpeg" class="apartment-gallery__slide">
                        &nbsp;</div>
                                    </div>
    <div class="apartment-gallery__pager">
        <div class="apartment-gallery__trigger apartment-gallery__trigger_prev js-gallery-prev"></div>
        <div class="apartment-gallery__trigger apartment-gallery__trigger_next js-gallery-next"></div>
    </div>
</div>
<!-- /gallery -->
        <!--Classified: bar-->
<div class="apartment-bar">
    <div class="apartment-bar__inner">
        <div class="apartment-bar__part apartment-bar__part_66">
            <div class="apartment-bar__item apartment-bar__item_price">
                <span class="apartment-bar__price apartment-bar__price_primary">
                    <span class="apartment-bar__price-value apartment-bar__price-value_primary">
                        192,85 р.
                    </span>
                </span>
                <span class="apartment-bar__price apartment-bar__price_secondary">
                    <span class="apartment-bar__price-value apartment-bar__price-value_complementary">
                        92 $
                    </span>
                </span>
            </div>
            <div class="apartment-bar__item">
                                    <span class="apartment-bar__value">Комната</span>
                
                            </div>
        </div>

        <div class="apartment-bar__part apartment-bar__part_33 apartment-bar__part_right">
            <div class="apartment-bar__item">
                                    <span class="apartment-bar__value">Собственник</span>
                            </div>
        </div>
    </div>
</div>
<!--Classified: bar END-->

        <div class="apartment-info">
            <div class="apartment-info__line">
    <div class="apartment-info__line-inner">
        <div class="apartment-info__cell apartment-info__cell_66">

            <!-- Classified: options-->
<div class="apartment-options">
            <div class="apartment-options__item">Мебель</div>
            <div class="apartment-options__item">Кухонная мебель</div>
            <div class="apartment-options__item">Плита</div>
            <div class="apartment-options__item">Холодильник</div>
            <div class="apartment-options__item">Стиральная машина</div>
            <div class="apartment-options__item">Телевизор</div>
            <div class="apartment-options__item">Интернет</div>
            <div class="apartment-options__item">Лоджия или балкон</div>
            <div class="apartment-options__item">Кондиционер</div>
    </div>
<!-- Classified: options END-->

            
        </div>

        <div class="apartment-info__cell apartment-info__cell_33 apartment-info__cell_right">
            <div id="apartment-phones" class="apartment-info__sub-line apartment-info__sub-line_extended-bottom_condensed-alter" style="display: none;"
                 data-bind="visible: true">
                <ul class="apartment-info__list apartment-info__list_phones"
                    data-bind="css: {'apartment-info__list_opened': $root.contactsVisible}">
                    <li class="apartment-info__item apartment-info__item_primary"
                        data-bind="click: $root.showContacts.bind($root)">
                    </li>
                                            <li class="apartment-info__item apartment-info__item_secondary">
                            <a href="tel:+37529000001">+375 29 000-00-01</a>
                        </li>
                                    </ul>
            </div>
            <div class="apartment-info__sub-line apartment-info__sub-line_extended apartment-info__sub-line_complementary">
                                Звоните в любое время
                            </div>
            <div class="apartment-info__sub-line apartment-info__sub-line_extended">
                <a href="https://profile.onliner.by/user/2787709"
                   onclick="window.ga && window.ga('ak.send', 'event', 'button', 'man_profile');">
                    Наталья</a>
                <br>
                            </div>

                            <div class="apartment-info__sub-line apartment-info__sub-line_extended">
                    <a href="https://chats.onliner.by/compose/2787709/ak/500310"
                       class="btn-flat btn-flat_dark-blue btn-flat_small-alter btn-flat_comment apartment-info__control"
                       target="_blank">
                        Написать автору
                    </a>
                </div>
            
            <div class="apartment-info__sub-line apartment-info__sub-line_extended apartment-info__sub-line_complementary">
                            </div>

                    </div>
    </div>
</div>

<script>
    (function () {
        var container = document.getElementById('apartment-phones'),
            isModerator = false,
            isOwner = false,
            popover = document.getElementById('contacts-popover'),
            viewModel = new ApartmentContacts('500310', popover, {
                visibleByDefault: isModerator || isOwner
            });

        container && ko.applyBindings(viewModel, container);
    }());
</script>

            <div class="arenda-block arenda-block_wide">
                <div id="adfox_154116814371877731"></div>
<script>
    window.Ya.adfoxCode.createAdaptive({
        ownerId: 260941,
        containerId: 'adfox_154116814371877731',
        params: {
            p1: 'ccnah',
            p2: 'fzvf',
            puid1: 'ak'
        }
    }, ['desktop'], {
        tabletWidth: 1000,
        phoneWidth: 640,
        isAutoReloads: false
    });
</script>

            </div>

            <div class="apartment-info__line">
                <div class="apartment-info__line-inner">
                    <div class="apartment-info__cell apartment-info__cell_66">
                                                <div class="apartment-info__sub-line apartment-info__sub-line_extended-bottom">
                            Жилье экономкласса.Койка-места. Кабинки. Сдаются.<br>
Рождественские скидки!Дешево! Не агент. Сдаётся спальное место самом центре города в общей комнате. Горячий душ, быстрый wi-fi, кухня и посуда. Пешком метро Немига, Купаловская, Октябрьская 3-5 минут, 6 минут до ж/д вокзала. Идеальный вариант для студентов на сессию, переночевать недорого или пожить на время командировки. Стоимость от 15 суток - 140 рублей, от 20 суток - 180 рублей. Уточняйте, пожалуйста, наличие свободных мест по телефону (мобильный + viber, whatsapp)
                        </div>
                                                <div class="apartment-info__sub-line apartment-info__sub-line_large">
                            Минск, Сторожовская улица, 8
                        </div>
                    </div>
                    <div
                        class="apartment-info__cell apartment-info__cell_no-border apartment-info__cell_33 apartment-info__cell_complementary apartment-info__cell_right">
                        <div class="apartment-info__sub-line apartment-info__sub-line_extended-bottom_condensed">
                            Размещено 2 дня назад
                        </div>
                        <div class="apartment-info__sub-line apartment-info__sub-line_extended-bottom_condensed">
                                                    </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="map" class="apartment-map"></div>
<script>
    (function () {
        var latitude = 53.9144843,
            longitude = 27.5537388,
            mapConfig;

        $.ajax({
            url: Onliner.secureProjectUrl('r', '/ak/assets/config/map.config.json'),
            async: false,
            dataType: 'json',
            success: function (response) {
                mapConfig = response;
            }
        });

        L.Icon.Default.imagePath = 'images/leaflet';

        var map = L.map('map', {
            center: [latitude, longitude],
            zoom: 16,
            minZoom: 10,
            attributionControl: false,
            zoomControl: false
        });

        L.tileLayer(mapConfig.tileSource, {
            detectRetina: true
        }).addTo(map);

        L.control.attribution({
            position: 'bottomright',
            prefix: false
        }).addAttribution(mapConfig.copyright).addTo(map);

        L.control.zoom({
            position: 'bottomleft'
        }).addTo(map);

        map.scrollWheelZoom.disable();

        L.marker([latitude, longitude], {
            icon: L.divIcon({
                className: 'map-marker',
                iconSize: [26, 26]
            })
        }).addTo(map);

    }());
</script>
    </div>
<script>
    $(function () {
        $.post('/ak/apartments/500310/views')
            .done(function () {
                $.get('/ak/apartments/500310/views').done(
                    function (response) {
                        response = parseInt(response, 10);
                        if (!isNaN(response)) {
                            $('.apartment-edit__views-counter').html(format_counter(response));
                        }
                    }
                );
            });

        var format_counter = function (count) {
            var text = format.pluralForm(count, ['просмотр', 'просмотра', 'просмотров']);

            return (count >= 10000 ? format.numberWithSpaces(count) : count) + ' ' + text;
        };
    });
</script>

</div>
</div>
<!--/Middle-->
</div><!-- /.l-gradient-wrapper -->
</div>
</div>
<!--/Container-->

<!--Bottom-->
<footer class="g-bottom">
    <div class="g-bottom-i">
        <div class="footer-style">
    <div class="footer-style__flex">
        <div class="footer-style__part footer-style__part_1">
            <ul class="footer-style__list">
                <li class="footer-style__item">
                    <a href="https://blog.onliner.by/about" class="footer-style__link footer-style__link_primary">
                        О компании
                    </a>
                </li>
                <li class="footer-style__item">
                    <a href="https://people.onliner.by/contacts" class="footer-style__link footer-style__link_primary">
                        Контакты редакции
                    </a>
                </li>
                <li class="footer-style__item">
                    <a href="https://b2breg.onliner.by/advertising" class="footer-style__link footer-style__link_additional">
                        Реклама
                    </a>
                </li>
                <li class="footer-style__item">
                    <a href="https://b2b.onliner.by/reg" class="footer-style__link footer-style__link_additional">
                        Размещение в каталоге
                    </a>
                </li>
                <li class="footer-style__item">
                    <a href="https://blog.onliner.by/vacancy" class="footer-style__link footer-style__link_primary">
                        Вакансии
                    </a>
                </li>
                <li class="footer-style__item">
                    <a href="https://blog.onliner.by/manifest" class="footer-style__link footer-style__link_primary">
                        Манифест
                    </a>
                </li>
                <li class="footer-style__item">
                    <a href="https://blog.onliner.by/siterules" class="footer-style__link footer-style__link_primary">
                        Пользовательское соглашение
                    </a>
                </li>
                <li class="footer-style__item">
                    <a href="https://blog.onliner.by/publichnye-dogovory" class="footer-style__link footer-style__link_primary">
                        Публичные договоры
                    </a>
                </li>
                <li class="footer-style__item">
                    <a href="https://support.onliner.by/" class="footer-style__link footer-style__link_primary">
                        Поддержка пользователей
                    </a>
                </li>
            </ul>
        </div>
        <div class="footer-style__part footer-style__part_2">
            <div class="footer-style__social">
                <a href="https://vk.com/onliner" rel="noopener" target="_blank" class="footer-style__social-button footer-style__social-button_vk"></a>
                <a href="https://facebook.com/onlinerby" rel="noopener" target="_blank" class="footer-style__social-button footer-style__social-button_fb"></a>
                <a href="https://twitter.com/OnlinerBY" rel="noopener" target="_blank" class="footer-style__social-button footer-style__social-button_tw"></a>
                <a href="https://youtube.com/onlinerby" rel="noopener" target="_blank" class="footer-style__social-button footer-style__social-button_yt"></a>
            </div>
            <div class="footer-style__copy">
                &copy; 2001&mdash;2019 Onliner.by
            </div>
        </div>
    </div>
</div>
    </div>
    <!--/Footer-->
</footer>
<!--/Bottom-->
</div>
<!--/Layout container-->

<script type="text/javascript" src="//gc.onliner.by/assets/openapi.4090947d0ecaab0e.js"></script>

<script type="text/javascript">
<!--//--><![CDATA[//><!--
var pp_gemius_identifier = new String("B8.gja_Lo5zCgGtl8IqkWZaGPzipEgeVWzlr0naEik7.i7");
//--><!]]>
</script>
<script type="text/javascript" src="https://gc.onliner.by/js/old-content/xgemius.js"></script><script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 924821186;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<div style="display:none">
<script type="text/javascript" src="//gc.onliner.by/assets/conversion.5052cdf9aceb628a.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/924821186/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
</div>

<link rel="stylesheet" href="//gc.onliner.by/assets/non-critical-styles.b9fd77c42ac078f1.css" importance="high"/>

<link rel="stylesheet" href="//gc.onliner.by/assets/auth.58a3ce4db06b64e0.css" importance="high"/>

<script type="text/javascript" src="//chats.onliner.by/assets/chats.f60412cd33ee5f14.js"></script>

<script type="text/javascript" src="//profile.onliner.by/assets/user-extended.f6d80c3df2afe435.js"></script>
</body>
</html>
"""

def test_get_phones():
    assert OnlinerPuller.get_phones(html) == ['+375290000001']