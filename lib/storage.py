import config
class DefaultStorage(dict):
    def set(self, k, v):
        self[k] = v
    def get(self, k):
        return self[k]

def Storage(collection_name, uri=None):
    if uri is None:
        uri = config.storage

    if uri.startswith("mongodb"):
        from .mongodb_storage import Storage
        return Storage(uri, collection_name)
    else:
        return DefaultStorage()