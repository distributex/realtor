#!/usr/bin/python3

import unittest
import json
import logging
import tempfile
import time
import config
from lib.subscription import SubscriptionService
from lib.objects import Apartment, Subscription, Type, Location


logging.basicConfig(level=logging.DEBUG)

def test_announce():

    a = Apartment()
    a.currency = 'USD'
    a.price = 200.0
    a.floor = 1
    a.link = 'https://rent.example.com/rent/1'
    a.id = '1'
    a.rooms = 1
    a.ts = int(time.time())
    a.type = Type.RENT_FLAT
    a.location = Location(
        address='Baker Street, London, UK',
        latitude=51.524259,
        longitude=-0.1584739)

    cases = []
    cases.append((Subscription(
        distance=5,
        budget=250,
        rooms=1,
        location=Location(
            address='Baker Street, London, UK',
            latitude=51.524259,
            longitude=-0.1584739)), True))
    cases.append((Subscription(
        distance=5,
        budget=250,
        rooms=2,
        location=Location(
            address='Baker Street, London, UK',
            latitude=51.524259,
            longitude=-0.1584739)), False))
    cases.append((Subscription(
        distance=5,
        budget=500,
        rooms=1,
        location=Location(
            address='Stationsplein, 1012 AB Amsterdam, Netherlands',
            latitude=52.3791283,
            longitude=4.8980833)), False))

    # test match function
    for s, expected_result in cases:
        print(s, expected_result)
        assert SubscriptionService.match(a, s)[0] == expected_result
        SubscriptionService.format_announce(a, s)