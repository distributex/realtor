#!/usr/bin/env python3
from lib import storage
import sys

if __name__ == "__main__":
    for name in  sys.argv[1:]: # ['subscriptions', 'apartments', 'apartments_pool', 'rent-onliner']:
        s = storage.Storage(name)
        print(name)
        for key, value in s.items():
            print(key, repr(value))

        print("")