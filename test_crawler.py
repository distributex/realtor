import pytest
import crawler
import config
from lib.objects import Apartment

@pytest.yield_fixture(scope='module')
def crawler_service():
    service = crawler.Crawler(config)
    yield service
    service.phones_usage_storage.drop()
    service.crawler_pool.drop()

def test_banned(crawler_service):
    a = Apartment()
    a.phones = ['+375000000001']

    for i in range(crawler.PHONE_RECORDS_ALLOWED):
        a.id = i
        assert crawler_service.on_new_apartment(a) == True

    assert crawler_service.on_new_apartment(a) == False
    