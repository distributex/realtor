#!/usr/bin/env python3

import logging
import sanic
import sys
from sanic import Sanic
from sanic_cors import CORS
import hashlib
import json
import os
import traceback

from lib.subscription import SubscriptionService
import config
from lib.storage import Storage
from lib.objects import Subscription, Type
from lib.apartments import Apartments

app = Sanic()

def run():
    config.setup()
    subscription_service = SubscriptionService(config)
    apartments = Apartments(config)
    CORS(app)

    @app.route(f"/dialogflow/{config.api_secret}", methods=["POST"])
    async def process_action(request):
        """Read more: https://api.ai/docs/fulfillment#request"""
        logging.info("webhook raw: %r\n%r\n", request.body, request.json)

        try:
            uid = request.json["id"]
            action = request.json["result"]["action"]
            source = request.json["originalRequest"]["source"]
            chat_id = round(request.json["originalRequest"]["data"]["message"]["chat"]["id"])
            username = request.json["originalRequest"]["data"]["message"]["chat"].get("username")
        except KeyError as e:
            return sanic.response.text(str(e), 400)

        if action == "subscribe-rent":
            try:
                p = request.json["result"]["parameters"]
                city = p.get("city")
                address = p.get("address")
                distance = float(p.get("distance"))
                budget = float(p.get("budget"))
                rooms = int(p.get("rooms"))
                currency = p.get("currency-name", "USD")

                str_distance_unit = p.get("distance-unit", "km")

                if str_distance_unit == "km":
                    distance_unit = 1.0
                elif str_distance_unit == "m":
                    distance_unit = 0.001
                else:
                    distance_unit = 1.0

                subscriber = subscription_service.subscribe(city, address, distance * distance_unit, budget, currency, rooms, source, chat_id, username, Type.RENT_FLAT)
                if subscriber:
                    apartments.forEach(lambda apartment: subscription_service.announce(subscriber, apartment))
                logging.info('added new subscription: %r', subscriber)
            except (ValueError, TypeError, KeyError) as e:
                logging.debug('not all arguments supplied: %s', e)
                return sanic.response.text(str(e), status=400)


        elif action == "unsubscribe":
            subscription_service.unsubscribe(source, chat_id)


        elif action == "subscribe-buy":
            try:
                p = request.json["result"]["parameters"]
                city = p.get("city")
                distance = float(p.get("distance"))
                budget = float(p.get("budget"))
                rooms = int(p.get("rooms"))
                currency = p.get("currency-name", "USD")
                str_distance_unit = p.get("distance-unit", "km")

                if str_distance_unit == "km":
                    distance_unit = 1.0
                elif str_distance_unit == "m":
                    distance_unit = 0.001
                else:
                    distance_unit = 1.0

                subscriber = subscription_service.subscribe(city, None, distance * distance_unit, budget, currency, rooms, source, chat_id, username, Type.SELL)
                if subscriber:
                    apartments.forEach(lambda apartment: subscription_service.announce(subscriber, apartment))
                logging.info('added new subscription: %r', subscriber)
            except (ValueError, TypeError, KeyError) as e:
                logging.debug('not all arguments supplied: %s', e)
                return sanic.response.text(str(e), status=400)


        elif action == "list-subscriptions":
            subscription_service.announce_subscriptions(source, chat_id, username)


        else:
            logging.warning("unknown action %s", action)
            return sanic.response.text("invalid action", status=400)

        return sanic.response.json(request.json["result"])

    app.run(host=config.host, port=config.port)

if __name__ == "__main__":
    run()