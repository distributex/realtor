import pytest
import threading
import requests
import time
import config
import api
import signal
import subprocess

rent_apartments = {'id': 'f479457d-31c7-4c84-9532-44ae2938c78f-19db3199', 'lang': 'ru', 'sessionId': 'e8ef5f55-0c3d-460d-8188-d38b35e454d0', 'timestamp': '2020-03-08T08:16:12.253Z', 'result': {'source': 'agent', 'resolvedQuery': '1', 'action': 'subscribe-rent', 'actionIncomplete': False, 'score': 1.0, 'parameters': {'rent-type': ['квартиру'], 'city': 'Минск', 'address': "станция метро Немига", 'distance-unit': 'km', 'distance': '5', 'budget': '300', 'rooms': '1', 'currency-name': ''}, 'contexts': [{'name': '__system_counters__', 'lifespan': 0, 'parameters': {'no-input': 0.0, 'no-match': 0.0, 'rent-type': ['квартиру'], 'rent-type.original': ['квартиру'], 'distance-unit': 'km', 'distance-unit.original': 'км', 'city': 'Минск', 'city.original': 'Минске', 'address': {'country': '', 'city': '', 'admin-area': '', 'business-name': 'станция метро', 'street-address': 'Немига', 'zip-code': '', 'shortcut': '', 'island': '', 'subadmin-area': ''}, 'address.original': 'станция метро Немига', 'distance': 5.0, 'distance.original': '5', 'budget': 300.0, 'budget.original': '300', 'currency-name': '', 'currency-name.original': '', 'rooms': 1.0, 'rooms.original': '1'}}, {
    'name': 'welcome', 'lifespan': 4, 'parameters': {'rent-type': ['квартиру'], 'rent-type.original': ['квартиру'], 'distance-unit': 'km', 'distance-unit.original': 'км', 'city': 'Минск', 'city.original': 'Минске', 'address': {'country': '', 'city': '', 'admin-area': '', 'business-name': 'станция метро', 'street-address': 'Немига', 'zip-code': '', 'shortcut': '', 'island': '', 'subadmin-area': ''}, 'address.original': 'станция метро Немига', 'distance': 5.0, 'distance.original': '5', 'budget': 300.0, 'budget.original': '300', 'currency-name': '', 'currency-name.original': '', 'rooms': 1.0, 'rooms.original': '1'}}], 'metadata': {'intentId': '25af21e9-764e-4d27-a238-4f5e12947ab8', 'intentName': 'subscribe-rent', 'webhookUsed': 'true', 'webhookForSlotFillingUsed': 'false', 'isFallbackIntent': 'false'}, 'fulfillment': {'speech': '', 'messages': [{'lang': 'ru', 'type': 0.0, 'speech': ''}]}}, 'status': {'code': 200, 'errorType': 'success'}, 'originalRequest': {'data': {'update_id': 363575830.0, 'message': {'message_id': 131067.0, 'from': {'id': 42963421.00000001, 'is_bot': False, 'first_name': 'John', 'username': 'john', 'language_code': 'en'}, 'chat': {'id': 42963421.00000001, 'first_name': 'Dennis', 'username': 'dzianis_v', 'type': 'private'}, 'date': 1583655372.0, 'text': '1'}}, 'source': 'telegram'}}
api_endpoint = f"http://localhost:{config.port}/dialogflow/{config.api_secret}"


@pytest.yield_fixture(scope='module')
def api_fixture():
    with open("api.log", "w") as log_fd:
        p = subprocess.Popen("./api.py", shell=True,
                             stdout=log_fd, stderr=log_fd)
        for _ in range(10):
            try:
                requests.get(api_endpoint)
                break
            except requests.exceptions.ConnectionError as e:
                print(e)
                time.sleep(1)
                continue
        else:
            raise Exception("failed to start an api service")
        yield
        p.kill()
        p.wait()


def test_rent(api_fixture):
    r = requests.post(api_endpoint, json=rent_apartments)
    print(r.status_code, r.text)
    assert r.status_code == 200
